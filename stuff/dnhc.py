from dnode import dnode
from dnh import DNH
from dnh_yfile import DNH_YamlFile
from abc import abstractmethod

    
class DNHC(DNH):
    """dnh for collection source"""
    
    def init(self,
            ):
        pass
     
    
    def get(self, key_path, root=None):
        self.pull_docs(key_path)
        return DNH.get(self, key_path)

    def set(self, key_path, value, deploy=True):
        self.pull_docs(key_path)
        return DNH.set(self, key_path, value)
        

    def getp(self, psg_pattern):
        self.pull_docs()
        return DNH.getp(self,psg_pattern)
                
    def setp(self, psg_pattern, value):
        self.pull_docs()
        return DNH.setp(self,psg_pattern, value)


    def pull_docs(self, key_path=None):
        if key_path is not None:
            key_parts = dnode._split_key(key_path)
        if key_path is None or key_parts[0] == '*':
            docs = self.get_all_docs()
            self.root_pre.set('', docs)
            self.root_new.set('', docs)
            
        else:
            doc_id_key_path = dnode._join_key(key_parts[:1])
            doc = self.get_doc(doc_id_key_path)
            self.root_pre.set(doc_id_key_path, doc)
            self.root_new.set(doc_id_key_path, doc)
    
    
    def deploy(self, value_dict=None):
        if not self.dry_run:
            self.save_file()  

    @abstractmethod
    def get_doc(self, doc_id):
        pass
    
    @abstractmethod
    def get_all_docs(self):
        pass


class DNHC_YamlFile(DNHC, DNH_YamlFile):

    def get_doc(self, doc_id):
        return DNH_YamlFile.get(self,doc_id)
    
    def get_all_docs(self):
        return DNH_YamlFile.get(self,'')
    


class DNHC_CDB(DNHC):
    
    pass

    


     
        
        
    


