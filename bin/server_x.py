#!/usr/bin/python
import flask
from cut.fileaccess import read_file, homedir
from flask import make_response
from cut.mixedtools import from_yaml_file
import json
import os
from flask.helpers import send_from_directory

p2_dir   = os.path.join(homedir(), 'code/cutpy/python2/') 

src_ac_path  = os.path.join(p2_dir, 'html', 'autocomplete_01.html') 

app = flask.Flask(__name__, static_url_path=p2_dir)

## https://jqueryui.com/autocomplete/#default
## https://stackoverflow.com/questions/20646822/how-to-serve-static-files-in-flask


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('js', path)


@app.route('/auto') 
def auto():

    content = read_file(src_ac_path)
    
    response = make_response(content)

    response.headers['Access-Control-Allow-Origin'] = '*'
    return response

#@app.route('/js/<filename>') 
def js(filename):
    content = read_file(os.path.join(p2_dir, 'js', filename) )

    response = make_response(content)

    response.headers['Access-Control-Allow-Origin'] = '*'
    return response



@app.route('/aa/<hut>') 
def stars(hut):
    
    content = "%s" % hut
    
    response = make_response(content)

    response.headers['Access-Control-Allow-Origin'] = '*'
    return response




app.run(debug=True, 
        port=3000,
        host='0.0.0.0')

