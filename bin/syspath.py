#!/usr/bin/env python3
import os,sys


def get_base_dir():
    script_path = os.path.join(os.getcwd(), __file__)
    script_path = os.path.abspath(script_path)
    script_dir  = os.path.dirname(script_path)
    base_dir    = os.path.dirname(script_dir)
   
    return base_dir
    
def extend_syspath():
    
    python_dir  = os.path.join(get_base_dir(), 'py')    
    if not python_dir in sys.path:
        sys.path.append(python_dir)

    