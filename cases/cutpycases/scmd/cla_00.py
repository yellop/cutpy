
from cutpytest.scmd.cmd_class_00 import CmdLineTestClass00
from cutpytest.scmd.test_base import TestBaseCmdLine
import os
from cut.mixedtools import to_yaml, to_yaml_safe, type_down

class Cla00(TestBaseCmdLine):


    @classmethod
    def setUpClass(cls):
        
        cmdline = CmdLineTestClass00()
        cls.cmdline = cmdline
            
    
    def test_get_arguments(self):
        
        #print(to_yaml_safe(self.cla.args))
        
        args = self.cmdline.main('--parse-cmdline-only start wak now --speed 500')
        
        argsd = self.cmdline.cla.args_to_dict(args)
        
        for key in argsd:
            #print("")
            #print(key)
            #print(getattr(args, key))
            #print( argsd[key])
            
            if type(argsd[key]) in [type(None), type(''), type(u''), type(0)]:
                #print(type(argsd[key]))
                self.assertEqual(getattr(args, key), argsd[key], key)
        
        
        
        
        
        
    
    
        
    
