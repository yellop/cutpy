from cutpytest.scmd.test_base import TestBaseCmdLine

class SCmd00(TestBaseCmdLine):
            
        
    def test_parse_00(self):
        
        self.call_ok(['--parse-cmdline-only', 'start', 'a', 'now'])
        
                
        
        
    def test_parse_01(self):

        self.call_ko(['--parse-cmdline-only', 'start', '-g', '6', 'b', 'soon'])        
        
        self.call_ok(['--parse-cmdline-only', 'start', '-g', '3', 'b', 'now'])        
        
        self.call_ok(['--parse-cmdline-only', 'start', '-g', '4', 'b', 'soon'])        
        

        self.call_ko(['--parse-cmdline-only', 'stop', '-S', 'b', 'now'])        
        
        self.call_ok(['--parse-cmdline-only', 'stop', 'b', 'now' ])        
        
        
        
        
