
from cutpytest.scmd.cmd_class_00 import CmdLineTestClass00
from cutpytest.scmd.test_base import TestBaseCmdLine
import glob
import os


class SCmdCompletion02(TestBaseCmdLine):


    @classmethod
    def setUpClass(cls):
        
        cmdline = CmdLineTestClass00()
        SCmdCompletion02.cla = cmdline.cla    
        for parser_func_name in cmdline.parser_funcs:
            getattr(cmdline, parser_func_name)()
        
        test_dirs_root = os.path.join(cls.base_dir, 'test_data', 'dirs')
        os.chdir(test_dirs_root)
        
        
        cls.cla = SCmdCompletion02.cla
        
        #print(glob.glob(os.path.join(test_dirs_root, '*')))
        #print(glob.glob('*'))
        

    
    def test_completion_dir_path(self):
                
            
        #print(glob.glob(os.path.join(test_dirs_root, '*')))
        #print(glob.glob('*'))
        
        proposals = self.completions(
            '--completion-call --completion-prefix c_ --dir')
        #print(proposals)
        
        self.assertIn('c__1/', proposals)
        
        ## if prefix is unique, next level is appended
        self.assertEqual(['--help', 'c__1/', 'c__1/d__1/'], proposals)
        
        
    
    def test_completion_path(self):
                
            
        
        proposals = self.completions(
            '--completion-call --completion-prefix c_ --dir')
        #print(proposals)
        
        self.assertIn('c__1/', proposals)
        
        ## if prefix is unique, next level is appended
        self.assertEqual(['--help', 'c__1/', 'c__1/d__1/'], proposals)
        
        
    
    