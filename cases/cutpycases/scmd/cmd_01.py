
from cutpytest.scmd.cmd_class_00 import CmdLineTestClass00
from cutpytest.scmd.test_base import TestBaseCmdLine
import os
import unittest

class SCmd01(TestBaseCmdLine):


    @classmethod
    def setUpClass(cls):
        
        cmdline = CmdLineTestClass00()
        SCmd01.cla = cmdline.cla    
        for parser_func_name in cmdline.parser_funcs:
            getattr(cmdline, parser_func_name)()
        
        
    
    def setUp(self):
        self.cla = SCmd01.cla
        
    
    #@unittest.skip('')
    def test_completion_00(self):
                
    
        proposals = self.completions('--completion-call --keep-woking-dir') 
        #print(proposals)
        self.assertIn('stop', proposals)
        
        
    def test_completion_01(self):
                
        
        
        proposals = self.completions('--completion-call start') 
        self.assertIn('--helmet', proposals)
        
    #@unittest.skip('')
    def test_completion_02(self):
    
     
        ## choices        
        proposals = self.completions('--completion-call start --gear')        
        self.assertIn('4', proposals)
        
        #print(proposals)
        

    #@unittest.skip('')
    def test_completion_03(self):
                
        
        
        proposals = self.completions('--completion-call start')
        assert 'steam' in proposals
    
    
    #@unittest.skip('')
    def test_completion_04a(self):
        
        context = 'start --dests 10 20  '
           
        proposals = self.cla.complete(context)
                
        self.assertIn('21', proposals)
        
    
    #@unittest.skip('')    
    def test_completion_04b(self):
                    
        context = 'start --dests 19 22 --gear '
        
        proposals = self.cla.complete(context)

        self.assertNotIn('21', proposals)
        

        
    
    
    def test_completion_dir_path(self):
                
        #print(self.base_dir)
        test_dirs_root = os.path.join(self.base_dir, 'test_data', 'dirs')
        os.chdir(test_dirs_root)
        
        #print(glob.glob(os.path.join(test_dirs_root, '*')))
        #print(glob.glob('*'))
        
        proposals = self.completions(
            '--completion-call --completion-prefix c_ --dir')
        #print(proposals)
        
        self.assertIn('c__1/', proposals)
        
        ## if prefix is unique, next level is appended
        self.assertEqual(['--help', 'c__1/', 'c__1/d__1/'], proposals)
        
        
    

    
    