# -*- coding: utf-8 -*-
import unittest
from cut.shell import CutShell
import os
import sys
import getpass
from cut.mixedtools import to_yaml
from collections import OrderedDict

class shell_00(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.output = ''
        assert os.sys.platform.startswith('linux')
        cls.cps = CutShell(verboseness='info',
                           log_func=cls.collect,
                           )
            
    
    
    
    def setUp(self):
        pass


    def test_show_x(self):
                
    
        self.clear_output()
        
        self.cps.cmd('ls /', show='Xo')
        
        self.assertTrue(self.output.count('exitst: '),self.output )
    
    
        self.clear_output()
        
        self.cps.cmd('ls /', show='x')
        
        self.assertFalse(self.output.count('exitst: '))


        self.clear_output()
        
        self.cps.cmd(r'ls /-', show='x')
        
        self.assertTrue(self.output.count('exitst: '))


    def test_show_c(self):

        self.cps.cmd(r'ls /-', show='c')
        
        self.assertTrue(self.output.count('cmdstr: '))
        
        #print(self.output)
    
   
 
    
    
    @classmethod
    def collect(cls, m, level=None):
        cls.output += "\n" + m
    
    @classmethod    
    def clear_output(cls):
        cls.output = ''
        
        