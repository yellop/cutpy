# -*- coding: utf-8 -*-
import unittest
import os, sys

from cut.mixedtools import tik, tok, to_yaml, xml_to_yaml, paint, shelloex
from cut.fileaccess import read_file, dir_up
from time import sleep
from unittest.case import SkipTest


class mixedtools_00(unittest.TestCase):
        
    def setUp(self):
        base_dir = dir_up(3)
        self.data_dir = os.path.join(base_dir,'test_data', 'xml')
        
          
    #@SkipTest 
    def test_mixed_00(self):
        
        tik()
        #print('')
        #print('probing stop watch')
        
        tik(key='A')
        tik(key='B')
        
        tok(pause=0,  mcs=True)
        
        sleep(0.1)
        tok(mcs=True)
        
        if os.path.isdir('/tmp') and os.closerange(4, 4440):
            pass
        
        tok(mcs=True)
                    
        tok(key='A', mcs=True)
        
        tok('',stream=sys.stderr)
        
        #print(to_yaml(tok.history))  # @UndefinedVariable
        
    
    def test_color(self):
        text  = 'Trara'
        text_ = paint(text, 'red')
        #print()
        #print(text)
        #print()
        
    
    
    #@SkipTest 
    def test_xml_to_yaml(self):
        
        #xml_path   = os.path.join(self.data_dir, 'simple.xml')
        xml_path   = os.path.join(self.data_dir, 'gconf-tree-da_small.xml')
        
        xml_string = read_file(xml_path)
        #print()
        #print(xml_string)
        
        yaml_string = xml_to_yaml(xml_string,
                                  colorful=1)
        yaml_string
        #print()
        #print(yaml_string)
        
    
    def test_shelloex(self):
        
        cmd = 'ls %s -a' % os.path.sep
        
        answer = shelloex(cmd)
    
        self.assertTrue(answer['stdout'])
        self.assertEqual(answer['exitst'], 0)
        
    
        #print(to_yaml(answer))
    
    def test_shelloex2(self):
        
        cmd = 'ls / -la | grep proc'
        
        answer = shelloex(cmd)

        print(to_yaml(answer))
        
    
    
    def test_shelloex3(self):
        
        cmd = 'echo "aa aa aa" | grep aa | wc'
        
        answer = shelloex(cmd)
        
        self.assertIn('3', answer['stdout'])
    
    
    
    
    def test_shelloex4(self):
        
        cmd = 'echo uu | ls ddd'

        answer = shelloex(cmd)
        
        self.assertEqual(answer['exitst'], 2)
    

    
    
    def test_shelloex5(self):
        
        cmd = 'echo -G uu | sleep 10'
        #cmd = 'sleep 10'
        answer = shelloex(cmd, bg=True)
        
        self.assertEqual(answer['exitst'], None)
    

    def test_shelloex6(self):
        
        cmd = 'achaha -G uu '
        answer = shelloex(cmd, bg=True, raise_cmd_not_found=False)
        
        self.assertEqual(answer['exitst'], 127)
            
        self.assertRaises(OSError, lambda : shelloex(cmd, bg=True))
   

    def test_shelloex7(self):
        
        
        cmd = 'ls lfo'
        
        self.assertRaises(Exception, lambda : shelloex(cmd, raise_exit_code=True))
        

