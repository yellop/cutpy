# -*- coding: utf-8 -*-
import unittest
import os
from os.path import join, islink, isfile, isdir, exists

from cut.fileaccess import dir_up, write_file
from cut.sync import sync_one_way
from cut.fileaccess import find
from cut.mixedtools import to_yaml, printy
import shutil
class sync_00(unittest.TestCase):
        
    @classmethod
    def setUpClass(cls):
        base_dir = dir_up(3)
        cls.data_dir_orig = os.path.join(base_dir,'test_data', 'sync_00')
        
    
    def setUp(self):
        self.dir_A = os.path.join(self.wdir, 'dir_A')
        self.dir_B = os.path.join(self.wdir, 'dir_B')
        
        if isdir(self.dir_A):
            shutil.rmtree(self.dir_A)
        shutil.copytree(self.data_dir_orig, self.dir_A, symlinks=True)    
        
        if isdir(self.dir_B):
            shutil.rmtree(self.dir_B)
            
        assert not exists(self.dir_B)
        
        
    
    def test_sync_00(self):

        sync_one_way(self.dir_A, self.dir_B, into=False)
        printy(find(self.dir_B))

        self.assertEqual(sorted(os.listdir(self.dir_A)),
                         sorted(os.listdir(self.dir_B)),)

        

        

    def test_sync_01(self):

        sync_one_way(self.dir_A, self.dir_B, into=True)
        
        printy(find(self.dir_B))
        
        self.assertEqual(os.listdir(self.dir_B), ['dir_A'])
        
        
    def test_sync_02(self):
        
        sync_one_way(self.dir_A, self.dir_B, 
                     out_of = True, into=True)
        
        #printy(find(self.dir_B))

        self.assertFalse(islink(join(self.dir_B, 'la')))
        
        
    
    def test_sync_03(self):

        sync_one_way(self.dir_A, self.dir_B, 
                     symlinks=True)
        
        #printy(find(self.dir_B))
        
        self.assertTrue(islink(join(self.dir_B, 'la')), self.dir_B)
        
        
        
           
        
    def test_sync_04(self):

        sync_one_way(self.dir_A, self.dir_B, into=False)
        
        new_A = join(self.dir_A, 'new_A')
        new_B = join(self.dir_B, 'new_B')
        write_file(new_A,'')
        write_file(new_B,'')
        
        sync_one_way(self.dir_A, self.dir_B, overwrite=True)
        
        
        printy(find(self.dir_B))

        #self.assertEqual(sorted(os.listdir(self.dir_A)),
        #                 sorted(os.listdir(self.dir_B)))     
        
        
