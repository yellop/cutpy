# -*- coding: utf-8 -*-
import unittest
import os, re
from cut.fileaccess import replace_in_file, save_file, read_file, strip_path, \
    afnmatch, find, dir_up


class fileaccess_00(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        base_dir = dir_up(3)
        cls.test_dir = os.path.join(base_dir,'test_data', 'dirs' )        
    
    def setUp(self):
        pass
    
    def test_peal_00(self):
        
        
        content_0 = """
tra: lalamu
hupd: selgma
000005555 --------- 555
"""
        
        path  = os.path.join(self.wdir, 'test_file_edit.txt')
        #self.assertTrue(False)
        
        
        save_file(content_0, path)
        
        replace_in_file(path, 
                  { 'lala': 'brum',
                    '\d{4}':  'X'   })
        
        content_1 = read_file(path)
        
        assert re.search('XX', content_1)
        #print(content_1)
        
    
    
        
    def test_strip_path(self):
        
        paths = [('.././xx/zz/', '../xx/zz'),
                 ('./xx/zz', 'xx/zz'),
                 ('/xx/zz/.', '/xx/zz'),
                 ('././/xx/zz/////.', 'xx/zz'),
                 ('xx/.', 'xx'),
                 
                 ]
        
        #print("\nAAAAAA test_strip_path\n")
        
        #a
        for path, spath in paths:
            assert strip_path(path) == spath, (path, strip_path(path) )
            
    def test_afnmatch(self):
        
        #print("\nAAAAAA test_fnamatch")
        
        
        path_0 = '/home/dir/ma/pli.py'
        pattern_0 = '**.py'
        pattern_1 = '/**/**/*.py'
        pattern_2 = '**/*.py'
        
        self.assertTrue(afnmatch(pattern_0, path_0))
        self.assertTrue(afnmatch(pattern_1, path_0))
        self.assertTrue(afnmatch(pattern_2, path_0))
        
        pattern_4 = 'home**.py'
        pattern_5 = '**/*b/*.py'
        pattern_6 = '*/*.py'
        
        self.assertFalse(afnmatch(pattern_4, path_0))
        self.assertFalse(afnmatch(pattern_5, path_0))
        self.assertFalse(afnmatch(pattern_6, path_0))
    
    def test_find_00(self):
        
        paths_all = find(self.test_dir)

        #print('\n'.join(paths_all))
        
        self.assertTrue('c__1/d_1' in paths_all)


        def into(x):
            return x[-1] == '1'

        paths_pruned = find(self.test_dir, descent_func=into)
        
        self.assertTrue('c__1/d_1' in paths_pruned)
        self.assertTrue('A/f1' not in paths_pruned)
        
        
        def hit(x):
            return x.count('__') == 0
        
        paths_no_du = find(self.test_dir, filter_func=hit)
        
        self.assertTrue('c__1' not in paths_no_du)
        self.assertTrue('A/f1' in paths_no_du)
        
        
        def into2(x):
            return x[-1] != 'a'
        
        paths_no_du_2 = find(self.test_dir, filter_func=hit, descent_func=into2)
        
        
        self.assertTrue('B/a' in paths_no_du_2)
        self.assertTrue('B/a/f1' not in paths_no_du_2)
        

        def hit2(x):
            return x.count(os.path.join(os.path.sep, 'a')) 
        

        paths_a_d = find(self.test_dir, filter_func=hit2, descent_into_hits=True)
        paths_a_n = find(self.test_dir, filter_func=hit2, descent_into_hits=False)

        #print("CVVVVVV")
        #print('\n'.join(paths_a_d))

        self.assertTrue('B/a/f1'     in paths_a_d)
        self.assertTrue('B/a/f1' not in paths_a_n)
        
        
        
        
        
        
        
        
    
      
        