# -*- coding: utf-8 -*-
import unittest
from cut.shell import CutShell
import os
import getpass
from cut.mixedtools import to_yaml
from collections import OrderedDict

class shell_remote(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        assert os.sys.platform.startswith('linux')
        
        cls.output = ''
                
        cls.cps = CutShell(verboseness='info',
                           log_func=cls.log,
                           remote_user = getpass.getuser(),
                           remote_host = 'localhost'
                           )
            
    
    def setUp(self):
        pass



    def atest_rcmd_renv_extra(self):
        
        
        env_extra = OrderedDict((('A', '9000'), ('B', 'fa la')))
        
        answer = self.cps.rcmdD(r"eval 'echo -n $A $B'", show='c', 
                                renv_extra=env_extra)
        
        
        self.assertEqual(answer['stdout'], ' '.join(env_extra.values()),)
        print(to_yaml(answer))
        
        
    def test_rcmd_rshell_00(self):
        
        answer = self.cps.rcmdD(r"echo $PATH", show='c', 
                                rshell = False)
        
        
        #print(to_yaml(answer))
        
        

        
        answer = self.cps.rcmdD(r"echo $PATH", show='c', 
                                rshell = True)
        
        
        #self.assertEqual(answer['stdout'], ' '.join(env_extra.values()),)
        #print(to_yaml(answer))
        
        
    @unittest.skip("cmd containing ' does not work with rshell")
    def test_rcmd_rshell_renv_extra(self):
        
        
        env_extra = OrderedDict((('A', '9000'), ('B', 'fa la')))
        
        answer = self.cps.rcmdD(r"eval 'echo -n $A $B'", show='c', 
                                renv_extra=env_extra,
                                rshell=True)
        
        
        #self.assertEqual(answer['stdout'], ' '.join(env_extra.values()),)
        print(to_yaml(answer))

        

    
    @classmethod
    def log(cls, m, level=None):
        cls.output += "\n" + m
    
    @classmethod    
    def clear_log(cls):
        cls.output = ''
        
        