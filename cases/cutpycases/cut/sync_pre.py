# -*- coding: utf-8 -*-
"""previous work on copy"""
import unittest
import os
from os.path import join, islink, isfile, isdir, exists

from cut.fileaccess import dir_up
from cut.fileaccess import find
from cut.mixedtools import to_yaml, printy, shelloex
import shutil



class sync_pre(unittest.TestCase):
    """Tests to find out and explain how other copy-tools behave"""
    
    @classmethod
    def setUpClass(cls):
        base_dir = dir_up(3)
        cls.dir_A_orig = os.path.join(base_dir,'test_data', 'sync_pre', 'A')
        cls.dir_B_orig = os.path.join(base_dir,'test_data', 'sync_pre', 'B')
        
    
    def setUp(self):
        self.dir_A = os.path.join(self.wdir, 'dir_A')
        self.dir_B = os.path.join(self.wdir, 'dir_B')
        
        if isdir(self.dir_A):
            shutil.rmtree(self.dir_A)
        shutil.copytree(self.dir_A_orig, self.dir_A, symlinks=True)    

        if isdir(self.dir_B):
            shutil.rmtree(self.dir_B)
        shutil.copytree(self.dir_B_orig, self.dir_B, symlinks=True)    
        
        print('\ndir A:')
        printy(find(self.dir_A, end_sep=True))
        
        print('\ndir B:')
        printy(find(self.dir_B, end_sep=True))

            

    def test_1_cp(self):
        """copy test"""
        sub_dir_A = join(self.dir_A, 'C') 
        sub_dir_B = join(self.dir_B, 'C') 
        
        assert isdir(sub_dir_B)
        print('\n' + sub_dir_B)
        shelloex(['cp', '-r', sub_dir_A, self.dir_B])
        printy(find(self.dir_B, end_sep=True))


    def test_2_copytree(self):
        
        sub_dir_A = join(self.dir_A, 'C') 
        sub_dir_B = join(self.dir_B, 'C') 
        
        assert isdir(sub_dir_B)
        print('\n' + sub_dir_B)
        try:
            shutil.copytree(sub_dir_A, self.dir_B)
            printy(find(self.dir_B, end_sep=True))
        except OSError as e:
            print(e)
        

        

        
