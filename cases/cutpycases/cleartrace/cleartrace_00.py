# -*- coding: utf-8 -*-
import unittest
#from unittest.case import SkipTest
from cleartrace.cleartrace import ClearTrace
from cut.mixedtools import to_yaml, to_yaml_safe



class cleartrace_00(unittest.TestCase):
         
    @classmethod
    def setUp(cls):
        cls.pt = ClearTrace(
                #auto_base_path=True,
                #base_path = '/home/nibo/code/cutpy/python2/py'
                )
    

    def test_trace_get(self):
        
        a = SomeClass()
        
        data = None
        try:
            a.inc('hu')
        except Exception:
            data = self.pt.get_error_data()
            
            ## trace starts with this class
            trace0 = data['trace'][0]
            
            self.assertIn(22, trace0['snippet_lines'])
            self.assertEqual(trace0['function_name'], 'test_trace_get')
             
            e_str = self.pt.form_yaml(data)
            self.assertTrue(e_str)
            #print(e_str)
            #print('')
        
        self.assertTrue(data)
        
        
    #@SkipTest 
    def test_trace_plain(self):
        
        a = SomeClass()
        
        error_str = None
        try:
            a.inc('hu')
        except Exception:
            data = self.pt.get_error_data()
            error_str = self.pt.form_plain(data)
        
        self.assertTrue(error_str)
        



class SomeClass(object):
    pass

    def inc(self, x):
        return x + 1
