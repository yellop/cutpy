# -*- coding: utf-8 -*-
import unittest
#from unittest.case import SkipTest
from cleartrace.cleartrace import ClearTrace
from cut.mixedtools import to_yaml, to_yaml_safe
from cutpytest.cleartrace.some_classes import SomeClass2
from cleartrace.testut import NoUnittest
from cleartrace.filter import SegmentPathFilter


class cleartrace_filter(unittest.TestCase):
         

    def setUp(self):
        
        self.segment_filter = SegmentPathFilter(
                                 #first_out='**some_classes.py' 
                             )
        
        self.pt = ClearTrace(
                        trace_filters= [ 
                            #NoUnittest,             
                            self.segment_filter
                        ],                
                )
    
    
    def make_error(self, show=False):
        data = None
        a = SomeClass2()
        try:
            a.inc('hu')
        except Exception:
            data = self.pt.get_error_data()
            if show:
                error_str = self.pt.form_plain(data)
                print(error_str)
        
        assert data
        return data
    
    
    def make_trace_list(self, show=False):
        data = self.make_error(show=show)
        trace = data['trace']            
        return trace

    def test_filtered_trace_0(self):
        
        self.segment_filter.first_out = None 
                
        trace = self.make_trace_list(show=0)
            
        self.assertEqual(len(trace), 3)
        self.assertEqual(trace[0]['class_name'], 'cleartrace_filter')
        self.assertEqual(trace[1]['class_name'], 'SomeClass2')
                     
        

    def test_filtered_trace_1(self):
        
        self.segment_filter.first_out = '**some_classes.py' 
        
        trace = self.make_trace_list(show=False)
            
        self.assertEqual(len(trace), 1)
        self.assertEqual(trace[0]['class_name'], 'cleartrace_filter')
                     


    def atest_filtered_trace_2(self):
        
        self.segment_filter.first_out = '**some_classes_no.py' 
        
        trace = self.make_trace_list(show=0)
        
        self.assertEqual(len(trace), 3)
        self.assertEqual(trace[0]['class_name'], 'cleartrace_filter')
        
        
                
    def test_filtered_trace_3(self):
        
        self.segment_filter.first_in  = '**some_classes.py' 
        
        trace = self.make_trace_list(show=0)
        
        self.assertEqual(len(trace), 2)
        self.assertEqual(trace[0]['class_name'], 'SomeClass2')
        

                
    def test_filtered_trace_4(self):
        
        self.segment_filter.last_out  = '**/cleartrace_filter.py' 
        
        trace = self.make_trace_list(show=0)
        
        self.assertEqual(len(trace), 2)
        self.assertEqual(trace[0]['class_name'], 'SomeClass2')  
        

    def test_filtered_trace_5(self):
        
        self.segment_filter.last_in  = '**/cleartrace_filter.py' 
        
        trace = self.make_trace_list(show=0)
        
        self.assertEqual(len(trace), 1)
        self.assertEqual(trace[0]['class_name'], 'cleartrace_filter')  
        



