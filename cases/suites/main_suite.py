from utest.discovery import UTestSuite


class MainSuite(UTestSuite):
    
    keys = [
        'fileaccess_00',
        'mixedtools_00',
        'cutpycases.utest.suite_00.Suite00',
        'cutpycases.scmd.cmd_00.SCmd00'  
    ]
    