# README #

Cutpy is a small tool-set for python.

### What is this repository for? ###

* set of mixed tools aiming comfort for coding python such as
    * mechanism for more comfortable inherence 
    * readable stop-watch 
    * readable traces

### Main Issues

* Many contained functions reinvent feature of existing tools.
* Some parts are quite mature, but there is also garbage code.

### How do I get set up? ###

* Include the module and classes you need in your code.
* The package cut is needed by the other packages, so you should always include the package cut.
* Imports are all done in advance, so you likely have to install some python-packages, even though you dont actually need them.

### Contribution guidelines ###

* leaf a message
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [Learn Hg](https://www.mercurial-scm.org/guide)

### Who do I talk to? ###

* repo owner
