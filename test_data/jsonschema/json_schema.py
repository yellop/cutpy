
from cut.fileaccess import read_file, save_file
import os
import jsonschema
from jsonschema import validate, _utils
from jsonschema.validators import validator_for, create, Draft4Validator

import json
import yaml

local_dir = os.path.dirname(__file__)

data_path   = os.path.join(local_dir,'json_schema_data_example.js')
schema_path = os.path.join(local_dir,'json_schema_example.js')

data_yaml_path   = os.path.join(local_dir,'json_schema_data_example.yaml')
schema_yaml_path = os.path.join(local_dir,'json_schema_example.yaml')



## translate to yaml
#data_json   = read_file(data_path)
#data        = yaml.load(data_json)
#data_yaml   = yaml.dump(data,default_flow_style=False)
#save_file(data_yaml, data_yaml_path)

data_yaml   = read_file(data_yaml_path)
data        = yaml.load(data_yaml)

## no json does not load non-floating-yaml
#data        = json.loads(data_yaml)
#data_json   = json.dumps(data)

#schema_json   = read_file(schema_yaml_path)
#schema        = yaml.load(schema_json)
#schema_yaml   = yaml.dump(schema,default_flow_style=False)
#save_file(schema_yaml, schema_yaml_path)
schema_yaml   = read_file(schema_yaml_path)
schema        = yaml.load(schema_yaml)
schema_json   = json.dumps(schema)


#print(data)
#print(schema)






validator_for(schema).check_schema(schema)


#def validate_(self, *args, **kwargs):
#    return  self.iter_errors(*args, **kwargs)



NewValidator  = create(meta_schema=_utils.load_schema("draft4"),
                       validators=(),
                       version="draft4"
                       )

class ValidatorProxy(object):
    
    def __init__(self, validator_dict):
        
        self.VALIDATORS = validator_dict
    
    def get(self, k):
        
        v = self.VALIDATORS.get(k) 
        
        def v_(validator, types, instance, schema):
            print("\n")
            print(k)
            print(instance)
            print(schema)
            if v:
                return v(validator, types, instance, schema)
            else:
                return None
        
        return v_
        


v1 = NewValidator(schema)

v1.VALIDATORS = ValidatorProxy(Draft4Validator.VALIDATORS)

E = v1.iter_errors(data)


#validate(data, schema, NewValidator)
#cls(schema, *args, **kwargs).validate(instance)





for e in E:
    print('')
    e
#    for k,v in e.__dict__.items():
        #print(k + ":\t" + str(v))
#        pass
        #print(v)
#validate(data, schema)
#validate([2, 3, 4], {"maxItems" : 2})


# Draft4ValidatorS = create(
#     meta_schema=_utils.load_schema("draft4"),
#     validators={
#         "$ref" : _validators.ref,
#         "additionalItems" : _validators.additionalItems,
#         "additionalProperties" : _validators.additionalProperties,
#     },
#     version="draft4",
# )
