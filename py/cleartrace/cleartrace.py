import sys, inspect, os, re
from collections import OrderedDict
from cut.mixedtools import to_yaml, to_yaml_safe
from math import floor
    

class ClearTrace(object):
    """Class to produce more readable traces when an exception is raised.
       It is derived from on ipythons ultratb written by Nathaniel Gray Fernando Perez.
       
       For a similar project see prettystack by Colm O'Connor
       https://pypi.python.org/pypi/prettystack/0.3.0
       
       
       Usage:
       
       ct = ClearTrace(...)
       
       try:
           ...
       except Exception:
           ct.show()
        
        
       
    """
    
    def __init__(self,
                 ## to shorten paths in trace entries
                 base_path=None,
                 auto_base_path = False,
                 show_locals    = False,
                 
                 ##
                 context_lines_pre    = 4,
                 context_lines_post   = None,
                 ##
                 
                 ## 
                 trace_filter  = None,
                 trace_filters = [],
                 
                 ##
                 field_filter  = None,
                 field_filters = [],
                 
                 line_number_width = 4,
                 
                 ):
        
        ## 
        self.base_path      = base_path 
        self.auto_base_path = auto_base_path
        self.show_locals    = show_locals
        self.context_lines_pre   = context_lines_pre
        self.context_lines_post  = context_lines_post
        self.line_number_width   = line_number_width
        
        
        ## 
        self.trace_filters = trace_filters
        
        if not trace_filters:               
            if trace_filter:
                self.trace_filters = [ trace_filter ] 

        ## 
        self.field_filters = field_filters
        
        if not field_filters:  
            if field_filter:
                self.field_filters = [ field_filter ] 

        
        ## 
        assert self.context_lines_pre == floor(self.context_lines_pre), self.context_lines_pre
        if context_lines_post is None:  
            self.context_lines_post = int(floor(self.context_lines_pre/8 + 1))    
                
    def show(self, 
             main=None, 
             form='plain', 
             filter_trace=True,
             filter_fields=True, 
             ):
        
        if not main:
            main = self.get_error_data()
        
        if filter_fields:
            self.apply_field_filters(main)
        if filter_trace:
            self.apply_trace_filters(main)
        
        
        if form == 'yaml':
            content = self.form_yaml(main)                                 

        if form == 'plain':
            content = self.form_plain(main)                                 

        print(content)
            

        
    def form_plain(self, error_data):
                
        out = ""
        for entry in error_data['trace']:
            
            out += entry['path'] + ':' + str(entry['line_number_error']) + '\n'
            
            ## TODO:
            if entry['snippet_lines']:
                
                snippet_str = self.join_with_line_numbers(
                            entry['snippet_lines'] , entry['line_number_error'])
        
                out += snippet_str + '\n'

            
            if self.show_locals:
                local_kv = OrderedDict()
                max_key_lenghts = 0
                for name, value in sorted(entry['local_vars'].items()):
                    local_kv[name] = self.ellipsis(value)
                    max_key_lenghts = max(max_key_lenghts,len(name))
            
                if local_kv:
                    for name, value in local_kv.items():
                        out += str(name) + ": " + " "*(max_key_lenghts - len(name)) + str(value) + '\n' 
           
                        
                    out += '\n'
        
        out += error_data['error_type'] + ': ' + error_data['message']
        
        return out
    
    
    def ellipsis(self, s, max_length = 80):
        if type(s) == type(''):
            s = repr(s)
        else:
            s = str(s) 
        if len(s) > max_length:
            s = s[:int(max_length * 2 / 3.)] + ' ... ' + s[-int(max_length * 1 / 3.):]
        return s
    
    def form_yaml(self, error_data):
       

        ## TODO: separate method for this
        excluded_trace_fields = [ 'class' ]

        for entry in error_data['trace']:
            for key in excluded_trace_fields:
                if key in entry:
                    entry.pop(key)
        
        
        
        out = to_yaml_safe(error_data)
        return out

    
    
    def get_error_data(self, exc_info=None, filter_fields=True, filter_trace=True ):
        
        if not exc_info:
            exc_info = sys.exc_info()
        
        etype, value, tb = exc_info
            
        records = inspect.getinnerframes(tb, context=24390314808)
        
        entries = [] 
        for frame, filepath, lnumber_error, function_name, lines, lnumber_rel in records:
            
            ##             
            self.line_number_width = max(self.line_number_width,
                                         len(str(lnumber_error + 
                                                 self.context_lines_post)))
            ## 
            entry = OrderedDict()
            
            ## path
            path = os.path.abspath(filepath)
            if self.base_path:
                path = os.path.relpath(path, self.base_path)
            entry['path'] = path 
            
            
            ##
            cls = self.get_class(frame)
            class_name = getattr(cls, '__name__', None)            
            entry['class']      = cls
            entry['class_name'] = class_name       


            ## function_name
            entry['function_name'] = function_name

            ## function with args    
            entry['function_head'], entry['arg_values'], \
            entry['local_vars'] = \
                self.get_function_call_info(frame, function_name)
            
                    
            ## lines
            ## TODO:
            snippet_lines = None
            try:
                snippet_lines = self.build_snippet_lines(
                                        lines, lnumber_error, function_name)
            except:
                pass
            entry['snippet_lines']     = snippet_lines
            entry['line_number_error'] = lnumber_error

            
            ## 
            entries.append(entry)


        out = OrderedDict()    
        
        
        ## basepath 
        self.set_base_path(entries)
        if self.base_path:
            out['base_path'] = self.base_path

        out['trace']       = entries
        out['error_type']  =  etype.__name__
        out['message']     =  str(value)
        
        if filter_fields:
            self.apply_field_filters(out)
        if filter_trace:
            self.apply_trace_filters(out)
        
        return out


    def set_base_path(self, entries):
        if not self.base_path:
            if self.auto_base_path:
                base_path = None
                if entries:
                    base_path = entries[0]['path']
                for entry in entries:
                    path = entry['path']
                    base_path = os.path.commonprefix([base_path, path])
                
                self.base_path = base_path

        if self.base_path:
            for entry in entries:
                path = entry['path']
                entry['path'] = os.path.relpath(path, base_path)
        


    def get_class(self, frame):
        argnames, vargs, okeyargs, local_vars = inspect.getargvalues(frame)
        ## class
        ## though convention based we try
        ## TODO: staticmethods
        cls        = None
        if argnames:
            if argnames[0] == 'self':
                obj = local_vars.get('self', None)
                if obj is not None:
                    cls = getattr(obj, '__class__', None)
            if argnames[0] == 'cls':
                cls = local_vars.get('cls', None)
        return cls
    
        ## https://stackoverflow.com/questions/2203424/python-how-to-retrieve-class-information-from-a-frame-object
        ## by Graham Dumpleton
#         def get_name( frame ):
#             code = frame.f_code
#             name = code.co_name
#             for objname, obj in frame.f_globals.iteritems():
#                 try:
#                     assert obj.__dict__[name].func_code is code
#                 except Exception:
#                         pass
#                 else: # obj is the class that defines our method
#                     name = '%s.%s' % ( objname, name )
#                     break
#             return name
#         
        
        
    def get_function_call_info(self, frame, function_name):
        
        argnames, vargs, okeyargs, local_vars = inspect.getargvalues(frame)        
        if function_name == '<module>':
            function_head = None
        else:
            argnames_  = argnames[:]
            if vargs:
                argnames_ += [ '*' + vargs ]
            if okeyargs:
                argnames_ += [ '**' + okeyargs ]

            function_head = function_name + "(%s)" % (", ".join(argnames_),)
        
        arg_values = OrderedDict()
        for i,arg_name in enumerate(argnames):
            if not ( i == 0 and arg_name in ['self', 'cls']):
                obj = local_vars.get(arg_name, None)
                arg_values[arg_name] = str(obj)
    

        local_vars_ = OrderedDict()
        for var_name in local_vars:
            if not var_name in ['self', 'cls']:
                obj = local_vars.get(var_name, None)
                local_vars_[var_name] = str(obj)
            

    
        return function_head, arg_values, local_vars_


    def build_snippet_lines(self, lines, lnumber_error, function_name):
        
        ## make lines-indices line-numbers 
        lines_ = [None] + lines
        
        ##
        context_lines_pre = self.context_lines_pre
        context_lines_post = self.context_lines_post
        lnumber_first     = max([lnumber_error - context_lines_pre,0])
        lnumber_last      = lnumber_error + context_lines_post
        
        
        ## 
        lnumber_header    = self.header_line_number(
                                 function_name, lnumber_error, lines)
        
        
        ## as dict
        lines_d = OrderedDict()
        
        if lnumber_header:
            lines_d[lnumber_header-1] = lines_[lnumber_header-1]    
            lines_d[lnumber_header]   = lines_[lnumber_header]    
            if lnumber_first == lnumber_header + 2:
                lines_d[lnumber_header+1]   = lines_[lnumber_header+1]   
            lnumber_first = max(lnumber_first, lnumber_header-1) 

        for i, line in enumerate(lines_[lnumber_first:lnumber_last+1]):
            lines_d[lnumber_first+i] = line
        
        
        
        
        
        return lines_d

    def header_line_number(self, function_name, lnumber, lines):
        
        lines_ = lines[lnumber:0:-1]
        
        out = None
        for i,line in enumerate(lines_):
            if re.match(r'\s*def\s+%s\s*\(.*' % function_name, line):
                out = lnumber - i + 1
                break
                
        
        return out
                
 
 
 
    def join_with_line_numbers(self, lines_d, lnumber_error):
        
            
        numbers_width = self.line_number_width
        out = ''
        last_number = None
        last_indent_size = None
        for number, line in lines_d.items():
            
            if line is not None:
            
                if last_number and number > last_number + 1:
                    number_part = '%*s'     % (numbers_width,'.' * len(str(last_number)))
                    indent_size = last_indent_size - 1 + 4
                    line_extra        = '%s   %s\n' % \
                        (number_part, 
                         (" " * indent_size)  + '...',
                         #'',
                         )
                    last_number = None
                    out += line_extra 
                else:
                    last_number = number
                    indent = re.sub(r'^(\s*).*', r'\1',line)
                    last_indent_size = len(indent)
                    
                if number == lnumber_error:
                    number_part  = '%*s'    % (numbers_width,number)
                    number_part +=  ' >'
                    line         = '%s %s'  % (number_part,line,)
                else:
                    number_part = '%*s'     % (numbers_width,number)
                    line        = '%s   %s' % (number_part, line)
                
                out += line 

        
        out = re.sub(r'\s+(\n|$)', r'\1', out)
        out = out + '\n'
        
        #out = unicode(out)
        
        return out    
        





     
    def apply_trace_filters(self, error_root):
        for trace_filter in self.trace_filters:
            trace_filter.apply(error_root)
            

    def apply_field_filters(self, error_root):
        for filed_filter in self.field_filters:
            filed_filter.apply(error_root)


        
