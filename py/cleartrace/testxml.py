from xmlrunner.runner import _XMLTestResult
from xmlrunner.result import  _TestInfo
from xmlrunner import XMLTestRunner

from .testut import NoUnittest
from .cleartrace import ClearTrace


class _XMLTestResultCT(_XMLTestResult):
    
    
    def __init__(self, 
                 clear_trace  = ClearTrace(
                                    trace_filters     = [ NoUnittest, ],
                                    field_filters     = [],
                                    context_lines_pre = 6
                                    ),
                 **args
                 ):
        
        super(_XMLTestResultCT, self).__init__(**args)
        
        self.ct = clear_trace
        
        
    def addFailure(self, test, err):
        
        ct             = self.ct                   
        
        error_root     = ct.get_error_data(exc_info=err)
        trace_str      = ct.form_plain(error_root)
        
        testinfo = _TestInfo(self, test, _TestInfo.ERROR, err)
        testinfo.test_exception_info = trace_str
        self.errors.append((
            testinfo,
            trace_str
        ))
        self._prepare_callback(testinfo, [], 'FAIL', 'F')


    def addError(self, test, err):
        
        ct             = self.ct                    
        
        error_root     = ct.get_error_data(exc_info=err)
        trace_str      = ct.form_plain(error_root)
        
        testinfo = _TestInfo(self, test, _TestInfo.ERROR, err)
        testinfo.test_exception_info = trace_str
        self.errors.append((
            testinfo,
            trace_str
        ))
        self._prepare_callback(testinfo, [], 'ERROR', 'E')



class XMLTestRunnerCT(XMLTestRunner):
    """TODO: buffer does not work, apparently _XMLTestResult does not
    support it. What we want: buffer=False means output appears in stdout 
    and xml-report as well"""
    
    def __init__(self, 
                 clear_trace = ClearTrace(
                                    trace_filters     = [ NoUnittest, ],
                                    field_filters     = [],
                                    context_lines_pre = 6
                                    ),
                 **args
                 ):
        
        super(XMLTestRunnerCT, self).__init__(**args)
        
        self.clear_trace = clear_trace        
        self.resultclass = _XMLTestResultCT
        
    def _make_result(self):
            
        return self.resultclass(
            stream        = self.stream, 
            descriptions  = self.descriptions, 
            verbosity     = self.verbosity,
            elapsed_times = self.elapsed_times,
            clear_trace   = self.clear_trace,                
            )

