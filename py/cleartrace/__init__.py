# -*- coding: utf-8 -*-
"""

Package to produce more readable traces when an exception is raised.
It is derived from on ipythons ultratb written by Nathaniel Gray Fernando Perez.
https://ipython.org/

For a similar project see prettystack by Colm O'Connor.
https://pypi.python.org/pypi/prettystack/0.3.0
       
       
Basic Usage:
    
    ## apply some controls here
    ct = ClearTrace(...)
       
    try:
       ...
    except Exception:
        ## build and print a readable trace from sys.exc_info()
        ct.show()


Data vs format:

        ## what-ever the output is it is based on a nested dict
        ## that holds information on the trace and that you get by
        error_description = ct.get_error_data()
        
        ## if you bring your own exc_info = sys.exc_info() 
        ## you pass to get_error_data
        error_data = ct.get_error_data(exc_info)
        
        ## format the data as you like, f.i. through a build-in formatter  
        error_string = ct.form_yaml(error_data)

Unittest-Integration:
    ## though the object ct is an instance we can use it multiple times
    ## e.g. pass it to the test-runner as is instead of crating
    ## one for each result
    
    ## TextTestRunner
    test_runner = TextTestRunnerCT(clear_trace = ct)
    ## XMLTestRunner
    test_runner = XMLTestRunnerCT(clear_trace = ct)



Copyright (C) 2010-2018 Lorenz Weizsäcker
The code of this package is released under the 
Apache License Version 2.0. a copy of which can be obtained 
at http://www.apache.org/licenses/LICENSE-2.0 .
"""


from .cleartrace import ClearTrace
from .testut import TextTestRunnerCT, TextTestResultCT, NoUnittest
from .testxml import XMLTestRunnerCT, _XMLTestResultCT
from .filter import FieldFilter, TraceFilter
