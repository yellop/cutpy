import unittest, sys
from unittest.runner import TextTestResult, TextTestRunner
from cleartrace import ClearTrace
from .filter import TraceFilter
from cut.mixedtools import stems_from_2, is_overwrite


class NoUnittest(TraceFilter):
    
 
    @classmethod
    def apply(cls, error_root):
        trace  = error_root['trace']
        trace_ = []
                
        for entry in trace:
            if 'function_name' in entry:        
                function_name = entry['function_name']
                #print(function_name) 
                
                if not hasattr(unittest.TestCase, function_name) and \
                   not hasattr(unittest.TestSuite, function_name) and \
                   not hasattr(unittest.case._Outcome, function_name) \
                   :
                    #print("APPEND 0")
                    trace_.append(entry)
                    continue
                    
                ## function overwritten?
                if 'class' in entry:
                    cls_entry = entry['class']
                    #assert hasattr(cls_entry, function_name)
                    if stems_from_2(cls_entry, unittest.TestCase): 
                        if hasattr(cls_entry, function_name) and hasattr(unittest.TestCase, function_name):
                            if is_overwrite(function_name, 
                                            cls_entry, unittest.TestCase):
                                #print("APPEND 1")
                                trace_.append(entry)
                    
        error_root['trace'] = trace_


              

class TextTestResultCT(TextTestResult):
    """
    Uses clear_trace to produce error-text when adding failures and errors.
    """


    def __init__(self, stream=None, descriptions=None, verbosity=None,
                 failfast=False, 
                 clear_trace =  ClearTrace(
                                    trace_filters     = [ NoUnittest, ],
                                    context_lines_pre = 6
                                    ),
                 ):
        
        assert clear_trace

        self.failfast = failfast

        super(TextTestResultCT, self).__init__(stream, descriptions, verbosity)

        self.failfast = failfast
        self.ct       = clear_trace

   
    def addError(self, test, err):

        ct             = self.ct                   
        
        error_root     = ct.get_error_data(exc_info=err)
        trace_str      = ct.form_plain(error_root)
        
        
        self.errors.append((test, trace_str))
        
        if self.showAll:
            self.stream.writeln("ERROR")
        elif self.dots:
            self.stream.write('E')
            self.stream.flush()

    def addFailure(self, test, err):

        ct             = self.ct                   
        
        error_root     = ct.get_error_data(exc_info=err)
        trace_str      = ct.form_plain(error_root)
        
        self.failures.append((test, trace_str))
        
        if self.showAll:
            self.stream.writeln("FAIL")
        elif self.dots:
            self.stream.write('F')
            self.stream.flush()


        
class TextTestRunnerCT(TextTestRunner):
    """
    Uses clear_trace to produce error-text when adding failures and errors.
    TODO: failfast option does not work
    """
    
    resultclass = TextTestResultCT

    
    def __init__(self, stream=sys.stderr, descriptions=True, verbosity=1,
                 failfast=False, buffer=False,
                 clear_trace = ClearTrace(
                                    trace_filters     = [ NoUnittest, ],
                                    context_lines_pre = 6
                                    ),
                 ):
    
        self.clear_trace = clear_trace
        self.failfast    = failfast
        
        super(TextTestRunnerCT, self).__init__(
              stream        = stream, 
              descriptions  = descriptions, 
              verbosity     = verbosity,
              failfast      = failfast,
              buffer        = buffer,
              )
        
        
        
    def _makeResult(self):
        
        result_object =  self.resultclass(
            self.stream, 
            self.descriptions, 
            self.verbosity,
            failfast        = self.failfast,
            clear_trace     = self.clear_trace,
            
            )

        result_object.failfast = self.failfast
        return result_object
        


