#!/usr/bin/python
import os, shutil 
from scmd.cmd_line import CmdLine
from utest.discovery import UTestDiscovery
from cut.mixedtools import from_yaml, iscontainer, to_yaml_safe
from cleartrace import ClearTrace, TextTestRunnerCT, NoUnittest

 
class TestCutPyCmdLine(CmdLine):

    
    def __init__(self,
             prog = 'test_cutpy',
             verboseness = 'info',
             base_dir_distance = 3,
             
             case_root_default = os.path.join('cases',),
             report_dir_default = os.path.join(os.getcwd(), 'reports'),
             
             **args
             ):
        super().__init__(prog=prog,
                         verboseness=verboseness,
                         base_dir_distance = base_dir_distance,
                         **args,
                         )
        self.case_root_default = case_root_default
        self.report_dir_default = report_dir_default
    
    def build_parser(self):
        
        ## main args
        
        self.define_argument('--case-root', '-B', 
                             action='store',
                             default=self.case_root_default,
                             help='case-root-dir(s) for searching cases',
                           )
        
        self.define_argument('--case-packages', '--case-dirs', '-C',
                             action='store',
                             help='skip all other cases-package',
                            )
        
        
        
        ## main
        self.update_argument('--verboseness',       '-v')
        
        
        self.add_arguments_main(
            '--case-root',
            '--case-packages', 
        )

        
        ## subcmd args

        
        case_keys_help="""specify tests with '.' between name parts
- test-case-name, example SCmd00 or scmd.cmd_00.SCmd00, 
- test of a case, example: SCmd00.test_parse
- module-name cmd_00 or scmd.cmd_00
- sub-dir, example scmd """

        
        self.define_argument('case_keys', 
                            type=str, 
                            nargs='+', 
                            action='store',
                            help=case_keys_help
                            )
        
                        
        
        self.define_argument('list_key', 
                            type=str, 
                            choices=UTestDiscovery.catalog_keys(),
                            action='store',
                            help='what to list'
                            )

        self.define_argument('--full-keys', 
                             '-f',
                             action='store_true', 
                             help='list full keys'
                            )
        
        
        
        
        
        ## subcmds      

        self.define_argument('--expand-keys-only', '-e', 
                             action='store_true',
                             help='dont run run test, show expanded keys only',
                            )
        
        self.define_argument('--report-dir', '-R',
                             help='where to save reports',
                             default =  self.report_dir_default,
                            )

        self.define_argument('--report-format', '-F', 
                             action='store',
                             default='text',
                             choices=['text','html','xml'],
                             help='output format of test-report',
                           )

        self.define_argument('--show-locals', '-l', 
                             action='store_true',
                             help='show local vars in traces',
                            )
        
        
        
        ##  
        self.add_subcmd('run', 
            help='run one or more test-cases',
            func=self._run,
            arguments = ('case_keys',
                         '--expand-keys-only',
                         '--report-dir', 
                         '--report-format',
                         '--show-locals',             
                         )
        )

        ## 
        self.add_subcmd('list', 
            help='list test items sich as known test cases',
            func=self._list,
            arguments = ('list_key',
                         '--full-keys'
                        )
        )

        ## 
        self.add_subcmd('catalog', 
            help='show full test catalog',
            func=self._catalog,
        )

        
    
       
    def run(self, args):
         
               
        ## 
        self.wdir = self.wdir
           
        
        ## we may want to allow multiple case roots
        ## however this is a bit advanced thus not recommended
        case_roots_raw = from_yaml(args.case_root)
        if not iscontainer(case_roots_raw):
            case_roots = [case_roots_raw]
        else:
            case_roots = case_roots_raw
        
        self.case_roots = []
        for case_root in case_roots:
            ## resolve case-root to dir
            case_root_path = None
            if os.path.isdir(case_root):
                case_root_path = case_root
            else:
                ## typical case
                case_root_path = os.path.join(self.base_dir,  
                                              case_root)
                
            
            if os.path.isdir(case_root_path):
                self.log("use case-root %s" % case_root_path)
                self.case_roots.append(case_root_path)
            else:
                raise Exception("Cannot resolve case-root %s" % case_root)
            
        
        ##
        self.case_packages = None 
        if args.case_packages:
            case_packages_raw = from_yaml(args.case_packages)
            if not iscontainer(case_packages_raw):
                case_packages = [case_packages_raw]
            else:
                case_packages = case_packages_raw
            self.case_packages = case_packages
            
        
        #print(self.case_dirs)
        self.utd = UTestDiscovery(log = self.log)
        
        
        self.test_collections  = self.utd.discover(
                                     self.case_roots,
                                     case_packages=self.case_packages)
        
        
        
        
        ##
        return args.func(args)
                
        
    def _list(self, args):
        
        self.log("list_key: %s" % args.list_key, 'debug')
        self.say(self.utd.list(self.test_collections, 
                            args.list_key, 
                            full_keys=args.full_keys))
        
       
    def _trail(self, args):
        
        root = os.path.join(self.base_dir, 'cases')
        cases = self.test_collections[root]['cases']
        print(self.test_collections[root].keys())
        
        
        for casekey, case in cases.items():
            print(casekey)
            print(case['doc'])
            
            #print(type(case['case']._tests[0]))
            
            
            print(case['tests'])
            for k,t in case['tests'].items():
                print(t['doc'])
            #print(case['case'].__doc__)
            
            
        
        out =  to_yaml_safe(cases ,
                            drop=(['_*', 
                                   'test', 
                                   'case',
                                   '*cut.*',
                                   '*clear*',                               
                                   
                                   ]
                                  ),
                            add_type=False)
        
        #self.say(out)

    def _catalog(self, args):
        
        #self._trail(args)
        
        self.say(self.utd.catalog_to_yaml(self.test_collections))
        
            
    def _run(self, args):
        pass      
        
        keys = args.case_keys[:]
        
        cases = self.utd.get_cases(keys, self.test_collections,
                                   )
        
        
        if args.expand_keys_only:
            for key in cases.keys():
                print(key)
        else:
            
            tests = self.utd.get_tests(cases.keys(), self.test_collections)
            
            for test in tests.values():
                self.log("initialize test: %s" % str(test))
                self.initialize_test(test)
            
            outfile = None
        
            report_dir = args.report_dir 
            if os.path.isdir(report_dir):
                shutil.rmtree(report_dir)
            os.makedirs(report_dir)
            
            clear_trace = ClearTrace(
                                     trace_filters= [ NoUnittest, ],
                                     #trace_filters= [],
                                     context_lines_pre = 8,
                                     show_locals=args.show_locals
                                     )
            
            suite = self.utd.build_unittest_suite(cases)
            if   self.args.report_format == 'html':
                test_runner, outfile = self.build_html_runner(report_dir)
            elif self.args.report_format == 'xml':
                from cleartrace.testxml import XMLTestRunnerCT
                test_runner = XMLTestRunnerCT(output=report_dir,
                                              clear_trace = clear_trace,
                                              #buffer = 0,
                                              verbosity=2)
                
            elif self.args.report_format == 'text':
                test_runner = TextTestRunnerCT(clear_trace=clear_trace,
                                               verbosity=2,
                                               )
            else:
                raise Exception('unkown report-format') 
            
            test_result = test_runner.run(suite)
            
            if outfile:
                outfile.close()
            
            return test_result
        
                
    def initialize_test(self, test):
        test.cli         = self
        test.wdir        = self.wdir
        test.base_dir    = self.base_dir
        test.log         = self.log
        test.__class__.log         = self.log
        test.__class__.base_dir = self.base_dir
        test.__class__.wdir = self.wdir
        test.__class__.base_dir = self.base_dir
        test.__class__.cli = self
    
    

          
        
        


