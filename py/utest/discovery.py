import sys, unittest, re, os, traceback
from importlib import import_module
from collections import OrderedDict
from cut.fileaccess import abspath
from cut.mixedtools import to_yaml_safe, shello, to_yaml
from fnmatch import fnmatch
from types import FunctionType



class UTestDiscovery():
    """unittest test discovery
    """ 
    
    def __init__(self,
             log = None,
             ):
        self.log = log
        
        
    def discover(self, 
                 ## serach within these dirs
                 case_roots,
                 ## restrict to these packages
                 case_packages=[],
                 strict = False,
                 ):
        """discover test from cases_dirs
        returns list of test-dict holding the test itself among various meta-info,
        see code
        """
        
    
        collections = OrderedDict()
        for case_root in case_roots:
            
            ## 
            self.log('start utest-discovery on\n%s' % case_root)
            
            ## normalize key
            case_root_abs = abspath(case_root)
            
            ## if case-dir is not  below an existing sys.path
            ## add case-dir to (and remove later from) sys-path
            ## if case-dir is below a syspath make it relativ
            ## to the longest sys-path containing it
            case_root_rel_sys_path = None
            spath_prefix           = None
            for spath in sys.path: 
                spath = abspath(spath)            
                if os.path.commonprefix([spath,case_root_abs]) == spath:
                    rel_spath = os.path.relpath(case_root_abs, spath)
                    rel_spath = os.path.normpath(rel_spath)
                    
                    ## if first or greater overlap 
                    if case_root_rel_sys_path is None or \
                        os.path.commonprefix([spath,spath_prefix]) \
                        == spath_prefix:
                        
                        spath_prefix           = spath
                        case_root_rel_sys_path = rel_spath   
            self.log('case-root rel to sys-path A: %s' % case_root_rel_sys_path, 'debug-1')     
            ## check whether rel_spath is complete package path 
            ## if not so we can either raise error or append case_root to sys-path
            if case_root_rel_sys_path is not None:
                if case_root_rel_sys_path != os.path.curdir:
                    syspaths_pre = sys.path
                    try:
                        sys.path = [spath]
                        package_path = '.'.join(os.path.split(case_root_rel_sys_path))
                        import_module(package_path)
                        self.log('package_path: %s' % package_path, 'debug-1')     
                        
                    except:
                        case_root_rel_sys_path = None
                        #raise Exception('not a packages-path: %s' % case_root_rel_sys_path)
                    finally:
                        sys.path = syspaths_pre  
            self.log('case-root rel to sys-path B: %s' % case_root_rel_sys_path, 'debug-1')     
            
            if case_root_rel_sys_path is None:
                spath     = case_root_abs
                sys.path = [ case_root_abs ] + sys.path                   
                    
            ## go
            collections[case_root_abs] = self.build_collection(case_root_abs,
                                                               package_prefixes=case_packages,
                                                               strict=strict)
            
            ## remove added sys.path
            if case_root_rel_sys_path is None:       
                sys.path.remove(case_root_abs)
                
                    
        return collections
    
    @classmethod
    def catalog_keys(cls):
        return ['packages', 'modules', 'cases', 'suites', 'simple-keys',
                'unimportable_modules' ]

  
      
    def build_collection(self, 
                         case_root_abs,
                         package_prefixes=[],
                         strict=False,
                         ):
        
        ## init coll
        coll = OrderedDict()
        for key in UTestDiscovery.catalog_keys():
            coll[key] = OrderedDict()
        
        self.log('case_root_abs: %s' % case_root_abs, 'debug-1')  
        
        ## go
        for dirpath, _, filenames in os.walk(case_root_abs):
            
            self.log('dirpath: %s' % dirpath, 'debug-1')
            
            if not '__init__.py' in filenames:
                continue
            
            ## package
            dirpath_rel  = os.path.relpath(dirpath, case_root_abs)
            self.log('dirpath_rel: %s' % dirpath_rel, 'debug-1')  
            package_path = '.'.join(dirpath_rel.split(os.path.sep))
            package_path_parts = package_path.split('.')
            package_path_parts_parent = package_path_parts[:-1]
            
            is_target_package = not package_prefixes or \
                    any([self.match_key_full(package_prefix, package_path, mode='prefix')
                         for package_prefix in package_prefixes])

            if not is_target_package:
                    self.log('package-path %s does not has prefix %s' % 
                                 (package_path, package_prefixes))
                    continue
            
            package = \
            coll['packages'][package_path] = OrderedDict()
            package['packages'] = []
            package['modules']  = []
            
            if package_path_parts_parent:
                ##     
                package_path_parent = '.'.join(package_path_parts_parent)
                if not package_path_parent in coll['packages']:
                    coll['packages'][package_path_parent] = OrderedDict()
                    coll['packages'][package_path_parent]['packages'] = []    
                package_parent = coll['packages'][package_path_parent]  
                package_parent['packages'].append(package_path)
                
            self.log('package_path: %s' % package_path, 'debug-1')  
            
            for filename in filenames:
                
                if fnmatch(filename, "*.py") and filename != '__init__.py':
                    
                    
                    ## module
                    filestem = os.path.splitext(filename)[0]
                    module_path = package_path + '.' + filestem
                    self.log('module_path: %s' % module_path, 'debug-1')     
                    module_file_path = os.path.join(dirpath, filename) 
                    try:
                        test_module     = import_module(module_path)
                    except ImportError as e:
                        
                        if strict:
                            print(to_yaml(sys.path))
                            for p in sys.path:
                                break
                                a = shello('find %s' % p)
                                for pp in a.split('\n'):
                                    print(pp)
                                  
                            raise e    
                        else:
                            error_dict = \
                            coll['unimportable_modules'][module_path] = OrderedDict()
                            error_dict['error_string'] = str(e)
                            error_dict['trace'] = traceback.format_exc()
                            error_dict['path'] = module_file_path
                            error_dict['exception'] = e
                            
                            self.log('test-discovery import error\n' +
                                     '%s\n%s\n' % 
                                     (
                                      #module_path,
                                      error_dict['path'],
                                      error_dict['error_string']
                                      ), 
                                     'info')
                            self.log(error_dict['trace'], 'debug')
                            self.add_simple_key(coll, filestem, module_path)
                        continue
                            
                            
                    load_unittest   = unittest.TestLoader().loadTestsFromModule(test_module, False) 
                    cases           = load_unittest._tests

                    coll['modules'][module_path] = OrderedDict((('cases', []),
                                                                ('suites', []),
                                                                ('doc', test_module.__doc__),
                                                               ))
                    ## cases
                    if cases:
                        for case in cases:
                            ucase = OrderedDict()
                            case_name = None 
                            ucase['case']       = case
                            ucase['tests']      = OrderedDict() 
                            test = None
                            for test in case._tests:
                                utest = OrderedDict()
                                case_name = test.__class__.__name__
                                test_func = getattr(test.__class__, test._testMethodName)
                                utest['test'] = test
                                if test_func.__doc__:
                                    utest['doc']  = test_func.__doc__.strip()
                                skipped = test_func.__dict__.get( '__unittest_skip__', False)
                                if skipped:
                                    utest['why_skipped'] = test_func.__dict__.get( '__unittest_skip_why__','')
                                #if test_linenumber
                                
                                test_func_ = UTestDiscovery.unwrapped(test_func)
				## TODO:
                                #if test_func_:                              
				#    utest['line_number'] = test_func_.__func__.func_code.co_firstlineno
                                ucase['tests'][test._testMethodName] = utest 
                            if case_name:    
                                case_key  = module_path + '.' + case_name
                                ## add only if it not an import 
                                if test and module_path == test.__class__.__module__:
                                    if test.__class__.__doc__:
                                        ucase['doc'] = test.__class__.__doc__
                                    coll['cases'][case_key] = ucase
                                    coll['modules'][module_path]['cases'].append(case_key)
                                    self.add_simple_key(coll, case_name, case_key)
                        package['modules'] += [ module_path ]
                    
                                  
                    ## suites
                    for k, o in test_module.__dict__.items():   
                        if not k.startswith('__'):
                            if isinstance(o, type)  and issubclass(o, UTestSuite) and \
                               o != UTestSuite:
                                suite_name      = k
                                suite           = OrderedDict()
                                suite['keys']   = o.keys
                                suite['keys_exclude'] = o.keys_exclude 
                                suite_key       = module_path + '.' + suite_name
                                coll['suites'][suite_key] = suite
                                coll['modules'][module_path]['suites'].append(suite_key)
                                self.add_simple_key(coll, suite_name, suite_key)

        return coll
            
    
    def add_simple_key(self, coll, simple_key, key ):
        if not simple_key in coll['simple-keys']:
            coll['simple-keys'][simple_key] = []
        if not key in coll['simple-keys'][simple_key]:   
            coll['simple-keys'][simple_key].append(key)
        
        
    def get_cases(self, keys, collections):
        
        ## the actual output, key_full -> case
        cases_out   = OrderedDict()
        ## additional mapping, key -> (case_root, key_full), to kept track 
        ## on how key was resolved, for logging 'already included' 
        added_ucti  = {}

        #for case_root_key, case_root in collections.items():
            #print(to_yaml(case_root['packages'].keys()))
        
        keys_exclude = []
        
        while keys:
            key = keys.pop(0)
            self.log("next key: %s" % key,)
            
            
            keys_new = []
            keys_exclude_new = []
            for case_root_key, case_root in collections.items():
            
                #self.log("AAA: %s" % str(case_root['unimportable_modules']))
                ## cases
                if key == '.':
                    case_root = collections.values()[0]
                    keys_new = case_root['modules'].keys()

                elif self.is_pattern(key):
                    
                    keys_new = self.matches_keys_full(key, case_root['cases'].keys())
                    
                    if not keys_new:
                        self.log('pattern %s has no match for %s' % \
                                 (key, case_root_key), 'warning')
                
                elif '.' not in key:
                
                    if key in case_root['simple-keys']:
                        keys_new = case_root['simple-keys'][key]
                

                       
                else:    
                    
                    key = re.sub(r'\.$','', key)
                
                    if key in added_ucti:
                        self.log("%s is already included from case-dir\n%s:%s" % \
                                 tuple([key] + added_ucti[key]) )
                
                    elif key in case_root['cases']: 
                        key_full = key
                        added_ucti[key_full]  = [case_root_key,key_full]
                        cases_out[key_full]   = case_root['cases'][key_full]['case']
                                        
                    
                    elif key in case_root['modules']:
                        key_full = self.match_keys_full(key, case_root['modules'].keys())
                        if key_full:
                        
                            keys_new  = case_root['modules'][key_full]['cases']
                            keys_new += case_root['modules'][key_full]['suites']
                            
                    elif key in case_root['packages']:
                        key_full = self.match_keys_full(key, case_root['packages'].keys())
                        if key_full:
                            #print(key_full)
                            keys_new  = case_root['packages'][key_full]['packages']
                            keys_new += case_root['packages'][key_full]['modules']
                    
                    
                    elif key in case_root['suites']:
                        key_full = self.match_keys_full(key, case_root['suites'].keys())
                        if key_full:
                        
                            keys_new          = case_root['suites'][key_full]['keys']                    
                            keys_exclude_new  = case_root['suites'][key_full]['keys_exclude']
                            ## TODO: prevent loops?
                            keys_exclude_new  = self.get_cases(keys_exclude_new, collections)   
                    
                    
                    elif key in case_root['unimportable_modules']:
                        error_dict = case_root['unimportable_modules'][key]
                        self.log('matching module found but import failed', 'info')
                        keys_new  = [key]
                     
                        raise error_dict['exception'] 
                        
                        
                    
            assert type(keys_new) is list, keys_new     
            
            #if not keys_new:
            #    self.log('no cases found for %s' % key)
                
            if key in keys_new:
                keys_new.remove(key) 
            if keys_exclude_new:
                for key_ in keys_new:
                    if key in keys_exclude_new:
                        keys_new.remove(key_)
                keys_exclude += keys_exclude_new
                       
            if keys_new:
                keys = keys_new + keys
                self.log_key_expantion( key, keys_new)
            

            
        cases_out_exclude = set()
                                    
        for key_exclude in keys_exclude:
            for case_key in cases_out:
                if case_key.startswith(key_exclude):
                    cases_out_exclude.add(case_key)
            
        cases_out_ = OrderedDict()
        for k,v in cases_out.items():
            if not k in cases_out_exclude:
                cases_out_[k] = v   
        
                   
                    
        return cases_out_
    
    @staticmethod
    def unwrapped(func):
        """Thanks to Martijn Pieters on stackoverflow."""
        if func.__closure__:
            closure = [c.cell_contents for c in func.__closure__]
            #print("DDDD1")
            #print(func.func_closure)
            
            return next((c for c in closure if isinstance(c, FunctionType)), None)
        
        else:
            #print("DDDD2")
            return func
  
    
    def log_key_expantion(self, key, keys_new):
            self.log('key-spec %s expanded to %s' % (key, ' '. join(keys_new)),
                     'debug')
                    
    
    
    def get_tests(self, case_keys, collections):
        
        tests_out = OrderedDict()
        for case_root in collections.values():
                
            cases = case_root['cases'] 
            
            for key_full in case_keys:
                
                case = cases[key_full]
                
                tests = case['tests']
                for key_test, test in tests.items():
                    key_full_test = key_full + '.' + key_test 
                    tests_out[key_full_test] = test['test']
                     
        return tests_out
    
    
    def build_unittest_suite(self, cases):
        
        suite = unittest.TestSuite()
              
        for case in cases.values():
            suite.addTests(case)
        
        return suite  
            
        
    
    
    
    
    
    
    def build_uctis(self, cases):
        """ucti: unittest case/test instance"""
        
        uctis = OrderedDict()
        for key_full, case in cases.items():
            uctis[key_full] = case['case']
            
            tests = case['tests']
            for key_test, test in tests.items():
                key_full_test = key_full + '.' + key_test 
                uctis[key_full_test] = test['test']
                
        return uctis
    
    
    def get_case_to_tests_map(self, suite):
        
        tests_dict      = OrderedDict()
        
        if hasattr(suite, '_tests') and suite._tests:
            for x_ in suite:
                tests_dict_ = self.peel_test_from_suite(x_)
                
                for case_name, test_list in list(tests_dict_.items()):
                    if case_name not in tests_dict:
                        tests_dict[case_name] = []
                    tests_dict[case_name] += test_list
        else:
            if hasattr(suite, '_testMethodDoc'):
                tests_dict[suite.__class__.__name__] = [suite]
                    
        return tests_dict


    def list(self, collections, what, full_keys=True, as_yaml=True):
        
        
        self.log('utest list %s' % what)
        
        collections_sub = OrderedDict()
        for case_root in collections:
            case_root_key  = os.path.normpath(case_root)
            collections_sub[case_root_key]   = collections[case_root_key][what]
    
        for case_root, items in list(collections_sub.items()):
            keys = list(items.keys())
            if not full_keys:
                keys = [ key.split('.')[-1] for key in keys ]
            collections_sub[case_root] = sorted(keys)
        
        if as_yaml:
            return to_yaml_safe(collections_sub)
        else:
            return collections_sub
        
    def catalog_to_yaml(self, collections):
        return to_yaml_safe(collections ,
                            drop=(['_*', 
                                   'test', 
                                   'case']
                                  ),
                            add_type=False)
        
        
    def build_package_trees(self,collections, doc_strings=True ):
        
        ptree = OrderedDict()
        
        
        ## TODO: not for first only
        catalog_root       = collections.values()[0]
        packages_root      = catalog_root['packages']
        modules_root       = catalog_root['modules']
        cases_root         = catalog_root['cases']
        
           
        def add_package(package_path, package_path_tail, dest_dict):
            if package_path_tail:
                
                path_parts = package_path_tail.split('.')
                
                if not 'packages' in dest_dict:
                    dest_dict['packages'] = OrderedDict()   
                
                                         
                if path_parts:
                    head = path_parts[0]
                    tail = path_parts[1:]
                    if not head in dest_dict['packages']:
                        dest_dict['packages'][head] = OrderedDict()
                        #dest_dict['packages'][head]['huhu'] = None 
                    path_tail = ".".join(tail)
                    add_package(package_path, path_tail, dest_dict['packages'][head])
                    

        def fill_package(package_key_parts, dest_dict):
            
            
            package_path = ".".join(package_key_parts)
            
            if package_key_parts:
                if doc_strings:
                    doc = packages_root[package_path].get('doc','hehe')
                    dest_dict['doc'] = doc
                
                modules = packages_root[package_path].get('modules',{})
                
                case_paths = []
                for module_path in modules:     
                    case_paths_ = modules_root.get(module_path, {}).get('cases',[])
                    case_paths.extend(case_paths_)
                
                cases_dict = OrderedDict()
                     
                for case_path in case_paths:
                    case_name = case_path.split('.')[-1]
                    case_dict = OrderedDict()
                    if doc_strings:
                        case_dict['doc'] = cases_root[case_path]['doc']
                        #case_dict['doc'] = 'DOC' + case_path              
                    cases_dict[case_name] = case_dict
                dest_dict['cases'] = cases_dict
                  
            
            for package_name_sub in dest_dict.get('packages',[]):
                
                package_key_parts_ = package_key_parts + [package_name_sub]
                dest_dict_         = dest_dict['packages'][package_name_sub]
                
                fill_package(package_key_parts_, dest_dict_)
                
            
            return
            

        ## groups: packages, modules, cases ,..                
        
        for package_path in packages_root.keys():
            add_package(package_path, package_path, ptree)
        
        fill_package([], ptree)
        
        return ptree
            
   


    def matches_keys_full(self, key, keys_full, mode='suffix'):
        out = []
        for key_full in keys_full:
            hit = self.match_key_full(key, key_full,mode=mode)
            if hit:
                out += [ key_full ]
        return out
        
 
    def match_keys_full(self, key, keys_full, mode='suffix'):
        for key_full in keys_full:
            hit = self.match_key_full(key, key_full,mode=mode)
            if hit:
                return key_full
        return None
    
    
    def match_key_full(self, key_spec, key_full, mode='suffix'):
        
        key_spec = re.sub(r'\.$', '', key_spec)
        key_spec = re.sub(r'^\.', '', key_spec)
        
        if key_spec == key_full:
            return True
        
        if mode == 'suffix':
            key_spec_ = '**.' + key_spec
        elif mode  == 'prefix':
            key_spec_ =  key_spec + '.**'
        else:
            raise Exception('unkown mode %s' % mode)
     
            
        return self.psg_match(key_spec_, key_full)

    def psg_match(self, psg_pattern, key_full):
        """point-sensitive globbing
        """
        pattern_0 = str(re.escape(psg_pattern))
        pattern_1 = re.sub(r'\\\*\\\*', '.*', pattern_0)
        pattern_2 = re.sub(r'\\\*', r'[^\.]*', pattern_1)
        pattern_3 = '(?s)' + pattern_2 + r'\Z'
        
        
        out =  re.match(pattern_3, key_full)
        #self.log(out)
        return out
    
    def is_module_file_name(self, fname):
        
        pattern = re.compile(r'[_a-z]\w*\.py$', re.IGNORECASE)        
        return pattern.match(fname) and not fname == '__init__.py'

    def is_pattern(self, key):
        return key.count('*')


        


class UTestSuite():
    """suites discovered by UTestDiscovery""" 
    
    keys = []
    
    keys_exclude = []
    
    
    




