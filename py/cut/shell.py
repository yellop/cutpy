# -*- coding: utf-8 -*-
import re, os, posixpath
from time import sleep
from cut.mixedtools import shelloex, iscontainer, is_string_like
from cut.fileaccess import save_file
import tempfile

class CutShell():
    """runs commands on local- and remote-host.

    related 
        https://github.com/amitt001/delegator.py
    """
    def __init__(self,
             remote_host    = None,
             remote_user    = 'root',
             rprofile       = None,
             wdir           = os.getcwd(),
             log_func       = None,
             verboseness    = 'debug',
             show_default   = 'coex',
             map_show       = {
                'debug-1':  'cOEXPt',
                'debug':    'coext',
                'info':     'coet',
                'warning':  '',
                'error':    '',
                'critical': '',
                },
             ):

        self.remote_host    = remote_host
        self.remote_user    = remote_user
        self.rprofile       = rprofile
        self.wdir           = wdir
        self.log_func       = log_func
        self.verboseness    = verboseness
        self.show_default   = show_default
        self.map_show       = map_show
        
                
        
    def log(self,message, level='debug'):
        if self.log_func:
            self.log_func(message, level=level)
        else:
            self.say(message)
   
    def say(self, message):
        print(message)
    
    def get_show(self, show=None):
        if show is not None:
            return show
        else:
            return self.map_show[self.verboseness]
    
    def get_log_func(self):
        def log_func(message):
            self.log(message, level=self.verboseness)
        return log_func
    
   
    def cmd(self, cmd, show=None, **options):
        """run cmds on localhost and return stdout"""
        D = self.cmdD(cmd, show=show, **options)
        return D['stdout']
    
    def cmdx(self, cmd, show=None, **options):
        """run cmds on localhost and return exit-code"""
        D = self.cmdD(cmd, show=show, **options)
        return D['exitst']


    def cmdD(self, cmd, show=None, **options):
        """run cmds on localhost and return a dict holding stdout, sdterr and more"""
        
        ## make shelloex to produce wanted outputs
        show  = self.get_show(show)
        
        ## make shoelloex to put it into log of self 
        ## with wanted level-arg
        options['print_func'] = self.get_log_func()
        D = shelloex(cmd, show=show, **options)
        
        return D 
    

    def rcmd(self, 
             cmd, 
             strip   = True,
             **args):
        """run cmds on self.host and return std-out"""
        
        oex = self.rcmdD(cmd, **args)
        
        out = oex['stdout']
        if strip:
            out = out.strip()
        return out


    def rcmdx(self, cmd, 
              **args):
        """run cmds on self.host and return std-out"""
        
        oex = self.rcmdD(cmd, **args)
        
        out = oex['exitst']
        return out

  
    def rcmdD(self, cmd, 
              show          = None, 
              sure_connect  = True,
              wait          = False, 
              rdir          = None,
              rprofile      = None,
              renv_extra    = None,
              rshell        = None, 
              rshell_opts   = '-l -c ',
              delta         = 4, 
              n_trial       = 50,
              **args):
        """run cmds on self.host and return a dict holding stdout, sdterr and more"""
        
        
        show = self.get_show(show)

        args['print_func'] = self.get_log_func()
        
        ##
        if iscontainer(cmd):
            cmd = " ".join(cmd)
        
        ##
        if rdir:
            cmd = ("cd %s; " % rdir )  + cmd

        
        ##
        if not rprofile:
            rprofile = self.rprofile    
        if rprofile:
            cmd = ('source %s; '% rprofile) + cmd
                
        ##
        if renv_extra:
            var_part  = ''
            for var_name, value in renv_extra.items():        
                var_part += "export " + str(var_name) + '='  + '"' + str(value) +  '"' + '; '
            cmd = var_part + " " + cmd

        ##
        ## get same env as after login 
        ## assuming remote SHELL knows -l and -c switches
        ## ssh localhost exec '$SHELL' -l -c \'echo $PATH\'
        ## ssh localhost 'echo $PATH'
        if rshell:
            if not is_string_like(rshell):
                rshell = "$SHELL"
            if rshell_opts is None:
                rshell_opts = ''
            else:
                assert is_string_like(rshell_opts)
                if not rshell_opts.endswith(' '):
                    rshell_opts += ' '
            
            ## TODO: this does not work
            #cmd = re.sub("'", r"\'", cmd)
            assert not cmd.count("'")
            
            cmd = "exec " + rshell + " " + rshell_opts + "'" + cmd + "'" 
            
            #print("")
            #print(cmd)
        
        ##
        if wait:
            self.wait_for_shell(delta, n_trial)    
        
        ##
        if 't' in show:
            show = re.sub('t', '', show)
            self.log("host: %s" % self.remote_host)
        
        ##
        if sure_connect:    
            no_auth_part = ['-o', 'UserKnownHostsFile=/dev/null', 
                            '-o', 'StrictHostKeyChecking=no' ]
        else:
            no_auth_part = []
        
        if 'c' in show:
            self.log("remote-cmd: " + cmd)
            if not 'C' in show:
                show = re.sub('c', '', show)
        
        assert self.remote_user, "need remote_user to be set"
        assert self.remote_host, "need remote_host to be set"
        
        ssh_cmd = ['ssh', '-q'] + no_auth_part + \
                  [self.remote_user + '@' + self.remote_host]
        
        
        #print(ssh_cmd + [cmd])
        
        oex = shelloex(ssh_cmd + [cmd],  
                       show=show,
                       **args)
        
        return oex


    def rpcmdD(self, cmd, **args):
        """run cmds as python cmds on self.host and return std-out"""
        
        if iscontainer(cmd):
            cmd = " ".join(cmd)
            
        cmd_py = '"%s"' % cmd
                
        answer = self.rcmdD('python -c %s' %  cmd_py, **args )
        
        return answer

    def rpcmd(self, cmd, **args):
        """run cmds as python on self.host and return std-out"""
        
        answer = self.rpcmdD(cmd, **args)
        
        return answer['stdout']

 

    def cplr(   self, source, rdest, 
                make_dir_remote = True, 
                sure_connect    = True, 
                show            = None, 
                ):
        """copy from local to remote, if rdest end with '/' source is copied 
        into the dir rdest """
        
        assert os.path.exists(source)
        
        show = self.get_show(show)

        
        rdest_ended_with_sep = rdest.endswith(posixpath.sep)
        
        rdest = posixpath.normpath(rdest)
        
        if rdest_ended_with_sep:
            if make_dir_remote:       
                self.rcmd(['mkdir', '-p', rdest ], show=show)
                        
        else:
            parent_dir = posixpath.dirname(rdest)
            
            cmdr = []
            if os.path.isdir(source):
                cmdr += ['rm', '-rf', rdest ]
            
            if make_dir_remote:     
                if cmdr:
                    cmdr += [';']
                cmdr += ['mkdir', '-p', parent_dir ]
            
            if cmdr:
                self.rcmd(cmdr, show=show)
        
        dest_url = self.remote_user + '@' + self.remote_host + ':' + rdest 
        
        if sure_connect:    
            no_auth_part = ['-o', 'UserKnownHostsFile=/dev/null', 
                            '-o', 'StrictHostKeyChecking=no' ]
        else:
            no_auth_part = []
        
        cmd = ['scp', '-rp', ] + no_auth_part + [ source, dest_url ]
        answer = self.cmdD(cmd, show=show, )
        return answer

        
    def cprl(self, rsource, dest, make_dir_local=True, show=None, sure_connect=True ):
        """copy from remmote to local, if dest end with '/' rsource is copied 
        into the dir dest """
        
        show = self.get_show(show)

        dest_is_dir = False
        if dest.endswith(os.path.sep):
            dest_is_dir = True
        
        dest = os.path.normpath(dest)
        parent_dir = os.path.dirname(dest)
        
        if dest_is_dir:
            parent_dir = dest + os.path.sep
            dest       = parent_dir
        else:
            parent_dir = os.path.dirname(dest)
        
        if not os.path.isdir(parent_dir) and make_dir_local:
            os.makedirs(parent_dir)
        
        if sure_connect:    
            no_auth_part = ['-o', 'UserKnownHostsFile=/dev/null', 
                            '-o', 'StrictHostKeyChecking=no' ]
        else:
            no_auth_part = []
                
        source_url = self.remote_user + '@' + self.remote_host + ':' + rsource 
        
        cmd = ['scp', '-rp', ] + no_auth_part + [source_url, dest ]
        answer = self.cmdD(cmd, show=show, )
        return answer

        

    def rscript(self, script, script_name=None, 
                interpreter='bash',
                remote_parent_dir='/tmp/hotfix/rscripts/',
                show=''):
        
        if not script_name:
            hash_int = abs(hash(script))
            hash_hex = "{:X}".format(hash_int).lower()
            script_name = 'shell_remote_script_{}'.format(hash_hex[:6])
        
        script_path   = os.path.join(self.wdir, script_name)
        script_path   = os.path.normpath(script_path)
        script_path_r = remote_parent_dir + '/' + script_name
        script_path_r = posixpath.normpath(script_path_r)
        
        save_file(script, script_path)
        self.cplr(script_path, script_path_r, show=show)
        self.log('\nremote script:\n{}\n{}'.format(
                        script_path,
                        self.remote_user + '@' + self.remote_host + \
                         ':' + script_path_r), 
                 
                  level='info')
        answer = self.rcmdD('{} {}'.format(interpreter,script_path_r),
                            show=show)
        return answer
        
    def read_rfile(self, path, show=None):
        
        return self.rcmd('cat {}'.format(path), show=show)

    def write_rfile(self, rpath, content):
        
        assert os.path.isdir(self.wdir)
        tmp_prefix = os.path.join(self.wdir, 'write_rfile_')
        tmp_path = tempfile.mktemp(prefix=tmp_prefix)
        save_file(content, tmp_path)
        self.cplr(tmp_path, rpath)
        
        return tmp_path


    def wait_for_shell(self, 
                       cmd = ['ls'], 
                       delta   = 6, 
                       n_trial = 100, 
                       output_mode = 'bool',
                       **args):
        """repeatetly try to run cmd on remote-host until the exit code is 0""" 
                
        assert output_mode in [ 'bool', 'stdout' ]
        out =  None  
        
        exit_code = 1
        n = 0
        while n < n_trial and exit_code:
            eox = self.rcmdD(cmd, **args)
            exit_code = eox["exitst"]
            if exit_code == 0:
                if output_mode == 'bool':
                    return True
                elif output_mode == 'stdout':
                    return eox['stdout']
                
            sleep(delta)
            n = n + 1
        
        
        return out








