import matplotlib.pyplot as pp
import numpy 
from math import sin, cos, log, e


def example_01():

    def f0(n):
        return ((n+1)/n)**n

    def f(n):
        return log(n,e)/n


    def g(n):
        return n**(1./n)



    # Data for plotting
    X = numpy.arange(1, 8, 0.1)
    F = [ f(x) for x in X ]
    G = [ g(x) for x in X ]

    fig, ax = pp.subplots()
    ax.plot(X, G, X, F)

    ax.set(xlabel='x', ylabel='y',
        title='exponent wins')
    ax.grid()

    #fig.savefig("test.png")
    pp.show()


if __name__ == "__main__":

    example_01()