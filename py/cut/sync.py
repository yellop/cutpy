import os, re
from os.path import exists, isfile, isdir, islink
import shutil
from collections import OrderedDict
from cut.mixedtools import to_yaml
from copy import deepcopy


class SyncException(Exception):
    def __init__(self, data, message=None):
        self.data = data
        self.message = message
    def __str__(self):
        out = ''
        if self.message:
            out = self.message
        
        out += '\n'  + to_yaml(self.data)
        return out


def remove(path, backup_root=None):
    
    assert not backup_root or isdir(backup_root) 
    
    if exists(path):
        if backup_root:
            if not isdir(backup_root):
                os.makedirs(backup_root)
            path_ = re.sub(r'^/+', '', path)
            path_new = os.path.join(backup_root, path_)
            shutil.move(path, path_new)
        else:
            if islink(path) or isfile(path):
                os.remove(path)
            else:
                shutil.rmtree(path)
                    

    
def sync_one_way( source, destin, 
              
              ##
              out_of    = None, ## source is parent
              into      = None, ## destin is parent
              
              ## 
              overwrite = False,
              symlinks  = False,
              
              ##
              backup_root = None,
              log_func    = lambda message: None, 
              
              ):
    
    ## future options
    #add       = True              
    #delete    = False
    #keep_stat = True
    
    
    
    ##
    args = OrderedDict()
    args['source'] = source
    args['destin'] = destin
    args['out_of'] = out_of
    args['into']   = into
    args['overwrite'] = overwrite
    args['symlinks']  = symlinks
    
    #args['delete'] = delete
    #args['keep_stat'] = keep_stat
    
    opts = deepcopy(args)
    opts.pop('source')
    opts.pop('destin')
    
    
    ## 
    assert not exists(source) or isdir(source) or isfile(source)
    assert not exists(destin) or isdir(destin) or isfile(destin)
    source_exists   = exists(source)
    source_isdir    = isdir(source)
    source_isfile   = isfile(source)
    #destin_exists   = ..
    #destin_isdir    = ..
    #destin_isfile   = ..
    ## case_number: 9
    
    
    ## set out_of and into by path-ending
    if out_of is None:
        out_of = source.endswith(os.path.sep)
    if into is None:
        into = destin.endswith(os.path.sep)
    ## case_number: 9 * 4 = 36
    
    ## overwrite
    ## case-number: 36 * 2 = 72
    

    
    ## prepare error
    error_data = deepcopy(args)
    error_data['out_of'] = out_of
    error_data['into']   = into
    error_data['source_exists']   = source_exists
    if source_exists:
        error_data['source_isdir']    = source_isdir
        error_data['source_isfile']   = source_isfile    
    def error(message=None):
        raise SyncException(error_data, message)

    def remove_ (path):
        log_func('remove %s' % path)
        assert overwrite
        remove(path,backup_root)

    def copy_( src, dest):
        
        dest_parent = os.path.dirname(dest)
        if not isdir(dest_parent):
            os.makedirs(dest_parent)
        
        assert not exists(dest), dest
        
        if islink(src):
            if symlinks:
                log_func('copy link %s to %s' % (src, dest))
                
                #print(dest)
                remove_(dest)
                link_target = os.readlink(src)
                #print(link_target)
                
                #print(exists(dest))
                os.symlink(link_target, dest)
                
                return
        
        if isdir(src):
            log_func('copy dir %s to %s' % (src, dest))
            shutil.copytree(source, dest, symlinks=symlinks)
        if isfile(src):
            log_func('copy file %s to %s' % (src, dest))
            shutil.copy2(source, destin)
    
    
            
    if exists(source):
        source_basename = os.path.basename(source)
        if into:
            destin_plus = os.path.join(destin, source_basename)
        else:
            destin_plus = destin
        
        error_data['destin_plus'] = destin_plus
        
            
        destin_parent = os.path.dirname(destin_plus)
        
        if not exists(destin_parent):
            os.makedirs(destin_parent)
        
        
        
        if exists(destin_plus):
            if overwrite:
                remove_(destin_plus)
            else:
                error("destin_plus exists, consider option 'overwrite'")
                
        ## DONE
        if isdir(source):
            if out_of:
                names = os.listdir(source)
                if names:
                    for name in names:
                        path = os.path.join(source, name)
                        opts['out_of'] = False
                        sync_one_way(path, destin, **opts)
                else:
                    error('out_of with empty source-dir')
            else:
                copy_(source, destin_plus)
                
                        
        
        ## DONE:
        if isfile(source):
            if out_of:
                error('cant copy out_of file')
            else:
                #print("FFF3") 
                copy_(source, destin_plus)
                
                    
    else:
        ## TODO:
        ## error as long assert not delete
        error('source does not exist')
        
                    
                    
                            
                

