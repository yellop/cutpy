from collections import OrderedDict
import logging.handlers, os, sys
import shutil

class LinePrefixAdapter(logging.LoggerAdapter):
    
    def process(self, msg, kwargs):
        lines = []
        for line in str(msg).split('\n'):
            lines.append(self.extra['prefix']+line)
        
        msg_ = '\n'.join(lines)
        return  msg_, kwargs


class Logging(object):
    

    def __init__(self,
                 name           = 'cutpylog',
                 logfile_path   = '/tmp/cutpy_log',
                 user           = None,
                 log_stdout_prefix = '',
                 
                 log_level      = 'debug',
                 verboseness    = 'warning',
                 
                 show_user       = True,
  
             ):


        self.log_level         = log_level
        self.verboseness       = verboseness
        self.logfile_path      = logfile_path
        self.user              = user
        self.log_stdout_prefix = log_stdout_prefix
        self.show_user          = show_user
        
        self.name_logfile   = name 
        self.name_stdout    = name + '.stdout' 
        self.name_stderr    = name + '.stderr' 
        
        self.setup_handler_logfile(logfile_path)
        self.setup_handler_stdout()
        self.setup_handler_stderr()
        
        
    
    
    def setup_handler_stdout(self):

        self.logger_stdout_plain = logging.getLogger(self.name_stdout)
    
        self.logger_stdout  = LinePrefixAdapter(self.logger_stdout_plain,
                                                 {'prefix':self.log_stdout_prefix})

        
        if hasattr(self, 'handler_stdout'):
            self.logger_stdout_plain.removeHandler(self.handler_stdout)
        
        lh  =  logging.StreamHandler(sys.stdout)
        
        lh.setLevel(self.numlevel(self.verboseness))
        
        form    = "%(message)s"
        formatter = logging.Formatter(form)
        lh.setFormatter(formatter)
        
        
        self.logger_stdout_plain.addHandler(lh)
        
        self.logger_stdout_plain.setLevel(self.numlevel(self.verboseness))
        
        ## we cannot use propagation because
        ## we do not want to propagate the added prefix
        self.logger_stdout_plain.propagate = False    
        
        self.handler_stdout = lh
        
    def setup_handler_stderr(self):
        self.logger_stderr = logging.getLogger(self.name_stderr)
        
        if hasattr(self, 'handler_stderr'):        
            self.logger_stderr.removeHandler(self.handler_stderr)
        
        lh  =  logging.StreamHandler(sys.stderr)
        lh.setLevel(self.numlevel('error'))

        form    = "%(message)s"
        formatter = logging.Formatter(form)
        lh.setFormatter(formatter)
        
        self.logger_stderr.addHandler(lh)
        
        self.logger_stderr.propagate = False
        self.logger_stderr.setLevel(logging.ERROR)
        
        self.handler_stderr = lh
        
            
        
    def setup_handler_logfile(self, path):
        
        
        self.logger = logging.getLogger(self.name_logfile)
        
        
        path_pre = None
        if hasattr(self, 'handler_logfile'):        
            hlf  = self.handler_logfile
            path_pre = self.logfile_path
            hlf.close()
            self.logger.removeHandler(hlf)
                
        parent_dir = os.path.dirname(path)
        if not os.path.isdir(parent_dir):
            os.makedirs(parent_dir)
        if path_pre and os.path.isfile(path_pre):
            shutil.move(path_pre, path)
        
        lh = logging.handlers.RotatingFileHandler(
            filename    = path, 
            maxBytes    = 16*1024**1024, 
            backupCount = 16,
            delay       = True)
        
        lh.setLevel(self.numlevel(self.log_level))
        
        if self.user and self.show_user:
            formatter = logging.Formatter(
                    "%(asctime)s %(levelname)s " +\
                     self.user + " \n%(message)s\n")
        else:
            formatter = logging.Formatter(
                    "%(asctime)s %(levelname)s \n%(message)s\n")
        
        lh.setFormatter(formatter)
        
        self.handler_logfile = lh
        
        self.logger.setLevel(self.numlevel(self.log_level))
        self.logger.addHandler(lh)
                
           
    def log(self, 
            message, 
            level="debug", 
            speak=False, 
            channel=sys.stdout):
        
        
        ## ??
        #if self.logger_stderr.isEnabledFor(level):
        if self.numlevel(level) >= self.numlevel('error'): 
            self.logger_stderr.log(self.log_levels[level], message)
        else:
            self.logger_stdout.log(self.log_levels[level], message)
            
        self.logger.log(self.log_levels[level], message)
        

    ## logging.DEBUG == 10
    ## logging.INFO  == 20
    
    log_levels = OrderedDict([ 
                ('debug-1',   logging.DEBUG - 1),
                ('debug',     logging.DEBUG),
                ('info',      logging.INFO),
                ('warning',   logging.WARNING),
                ('error',     logging.ERROR),
                ('critical',  logging.CRITICAL), 
    ])


    def numlevel(self, level_string):
        return self.log_levels[level_string.strip().lower()]
