import collections
from operator import itemgetter
from collections import OrderedDict


def sort_A_by_B(a,b,reverse=False, out="a"):
    """Sort A by permutation that sorts B"""
    
    if not len(a) == len(b):
        raise Exception()
    z = zip(a,b)
    
    sorted_pairs = sorted(z, key=itemgetter(1), reverse=reverse )
    
    if out == "a":
        return list(map(itemgetter(0), sorted_pairs))
    if out == "ab":
        #print sorted_pairs
        return sorted_pairs
    


def sort_A_by_b(A,b):
    """Sort list A by partial linear order given in b. Elements not in b,
    are come after elements in b keeping their order given in A
    
    TODO: use another name. The name is similar to sort_A_by_B but
    the functions are very different.""" 
    
    
    A_       = A[:]
    #[::]
    
    A_sorted = []
    
    for x in b:
        if x in A_:
            A_.pop(A_.index(x))
            A_sorted += [ x ]
    
    A_sorted += A_
    
    return A_sorted
         
    
def sorted_dict(a_dict, reverse=False):
    
    keys   = a_dict.keys()      
    values = a_dict.values()
    
    keys_sorted   = sort_A_by_B(keys, values, reverse=reverse)
    
    out = OrderedDict()
    
    for key in keys_sorted:
        out[key] = a_dict[key]
    
    return out

def inversed( mapping, force=False):
    map_   = type(mapping)()
    for key, value in map.items():
        if value in map_:
            if not force:
                raise Exception('cannot invert mapping, values not unique')
            else:
                ## first come first serve
                pass
        else:
            map_[value] = key
    return map_
    
    
def max_indices(b, k=1):
    
    a = list(range(len(b)))
    
    sorted_indices = sort_A_by_B(a,b,out="a", reverse=True)
        
    return sorted_indices[0:k]


        
def frequencies(seq, mapping = lambda x: x, 
                sort_by_frequency=False, relativ=False):
    """Returns list of (value, frequency) pairs where frequency 
    is the number of occurances of value in seq after mapping 
    is applied to each entry. The pairs are sorted by value.
    
    If relativ == True, frequency is normalized. 
    If sort_by_frequency == True, the list is sorted by frequency.
    """ 
    
    if sort_by_frequency: 
        sort_key = itemgetter(1)
        reverse  = True
    else:
        sort_key = itemgetter(0)
        reverse  = False
        
    d = collections.defaultdict(lambda: 0)
    
    n = len(seq)
    
    for item in seq:
        if relativ:
            d[mapping(item)] += 1./n
        else:
            d[mapping(item)] += 1
            
    return sorted(iter(d.items()), key=sort_key, reverse=reverse)


def most_frequent_item(seq):
    if seq:
        return frequencies(seq,  sort_by_frequency=True)[0][0]
    else:
        return None
    
def flatten_list(nested_list):
    """Makes a list out of a nested list"""
    out = []
    for a_list in nested_list:
        out.extend(list(a_list))
    return out

  
def reverse(string):
    return string[::-1]


def transpose_list(rows):
    """Test wether the nested list rows has matrix form 
    and if so the columns becomes rows and vis versa."""
    
    # test wether it is a matrix
    n_rows    = len(rows)
    if n_rows == 0:
        return []
    else:
        n_columns = len(rows[0])
    
    for row in rows:
        if len(row) != n_columns: 
            print('row lenghtes do not match.')
            return 1
    
    # get columns 
    columns = []
    for j in range(0,n_columns):
        columns.append([])
        for row in rows:
            columns[j].append(row[j])
   
    return columns



def list_successor(liste, entry):
    """Returns entry in the next position after the first posistion of entry.
    If entries are unique, this is the next value."""
    try:
        return liste[liste.index(entry) + 1]    
    except IndexError:
        return None
        
            
def indices(indexable):
    return list(range(len(indexable)))

def has_unique_value(A):
    """checks wether all entries in list A are equal"""   

    if len(A) == 0:
        return True
    else: 
        a = A[0]
        return all ( [x == a for x in A] ) 


def non_equal_elements(A):
    """Tests wether all elements of A are different w.r.t. to '=='"""
    as_list = list(A)
    
    n       = len(as_list)
    
    for i in range(n):
        for j in range(i+1,n):
            if as_list[i] == as_list[j]:
                return False
    
    return True        
 
def distinct_entries(V, order='appearance'):
        """return lists of distinct values in V
        in order of appearance"""
        
        values = []
        
        if order == 'appearance':
            for v in V:
                if not v in values:
                    values += [ v ]
        else:
            raise NotImplemented()
        
        return values

def list_cap(seq_a, seq_b):
    cap = []
    for a in seq_a:
        if a in seq_b:
            cap.append(a)
    return cap

def weak_dict_update(defaults, provided_dict):
    """Return dict with keys from defaults. The values are taken
    from provided_dict if key exists and from defaults otherwise"""
    out = {}
    
    for key in list(defaults.keys()):
        
        if key in provided_dict:
            out[key] = provided_dict[key]
        else:
            out[key] = defaults[key]
            
    return out

def peel(container):
    """Peel outermost brackets away.""" 
    if type(container) in [list, tuple] and len(container) == 1:
        return peel(container[0])
    else:
        return container

def multi_range( sizes, names=None):
    
    def rec(sizes):
        out = []
        for i in range(sizes[0]):
            if len(sizes)>1:
                for R in rec(sizes[1:]):
                    out +=  [ [i] + R ]
                    
            else:
                out += [ [i] ]
        return out
    
    sizes = list(sizes)
    out_  = rec(sizes)
    out_  = [ tuple(mindex) for mindex in out_ ]
    
    if names:
        assert len(sizes) == len(names)
        out_ = [ OrderedDict(list(zip(names,mindex)) ) for mindex in out_ ]
    
    return out_
        


if __name__ == '__main__':
    
    
    if 1:
        A = [1, 2, 3, 2, 5,5,3,5]
        B = [33, 2 ,2, 1,20]
        print((most_frequent_item(A)))
 
        
 
    if 0:    
        a =  [4, 3, 54, 23, 1 ,54, 0, 54,2] 
        c = [float(x) for x in a] 
        #b = better_list(c)
        
        
