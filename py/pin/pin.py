import os, subprocess
from cut.fileaccess import dir_up, save_file
from cut.mixedtools import shello
from PyInstaller.building.build_main import Analysis
from PyInstaller.building.api import PYZ, EXE
#from PyInstaller.config import CONF
class Pin(object):
    
    def __init__(self,
                 path,
                 base_dist = 1,
                 mains = [],
                 pathex = [],
                 datas = []
                 ):
        
        self.path     = os.path.join(path)
        
        
        self.base_dir = dir_up(base_dist, self.path)
        self.filename = os.path.basename(self.path)
        self.name     = os.path.splitext(self.filename)[0] 
        
        self.mains  = [ path ]
        self.pathex = [ os.path.join(self.base_dir,p) for p in pathex ]
        self.datas  = [ ( os.path.join(self.base_dir,s), 
                          os.path.join(self.name, d)) 
                        for s,d in datas ]
        
        #self.workpath = os.path.join(self.base_dir, 'build', self.name)
        #self. = os.path.join(self.base_dir, 'build', self.name)
        #CONF['workpath'] = self.workpath
        
        
        version_cmd    = ['python3', self.path, '--version']
        version        =  shello(version_cmd).strip()
        
        self.name_exec    = self.name + '-' + version         
        
        
        
    
  

    def make_spec(self, spec_dir=None):
        pass
        spec_tpl = """# -*- mode: python -*-

a = Analysis({mains},
             pathex        = {pathex},
             binaries      = None,
             datas         = {datas},
             hiddenimports = None,
             hookspath     = None,
             runtime_hooks = None,
             excludes      = None,
             win_no_prefer_redirects = None,
             win_private_assemblies  = None,
             cipher                  = None)

    
pyz = PYZ(a.pure, 
          a.zipped_data,
          cipher=None)
             
             
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='{name_exec}',
          debug=False,
          strip=None,
          upx=True,
          console=True )
"""
        
        spec = spec_tpl.format(
            mains= self.mains,
            
            pathex= self.pathex,
            datas= self.datas,
            name_exec= self.name_exec,
                                )
        
        if spec_dir:
            if not os.path.isdir(spec_dir):
                os.makedirs(spec_dir)
            save_file(spec, os.path.join(spec_dir, self.name + '.spec'))
        else:
            print(spec)



