import unittest, os
from cut.mixedtools import shelloex, is_string_like

class TestBaseCmdLine(unittest.TestCase):
    
        
    def call(self, arguments):
        
        this_path = os.path.abspath(__file__)
        this_dir  = os.path.dirname(this_path)
        script_path = os.path.join(this_dir, 'bin', 'test_scmd.py')
        
        tests_dir   = os.path.dirname(this_dir)
        py_dir      = os.path.dirname(tests_dir)
        
        
        if is_string_like(arguments):
            arguments  = arguments.split()
        #print('call test_scmd %s' % " ".join(arguments))
        cmd = [script_path] + arguments
        
        
        return shelloex(cmd, output='dict', 
                        env_extra={'PYTHONPATH': py_dir },
                        #show='c',
                        )
    
    
    
    
    def call_ko(self, arguments):
        
        answer =  self.call(arguments)
        self.assertNotEqual(answer['exitst'], 0)
        if not answer['exitst']:
            print(answer['stdout'])
            print(answer['stderr'])
            
        
        return answer


    def call_ok(self, arguments):
        
        answer =  self.call(arguments)
        
        if answer['exitst']:
            print(answer['stdout'])
        
            print(answer['stderr'])
        
        self.assertEqual(answer['exitst'], 0)
        
        return answer
    
    def completions(self, args):
        
        answer = self.call_ok(args)
        
        proposals = answer['stdout'].split()        
        
        
        return proposals