from scmd.cmd_line import CmdLine

class CmdLineTestClass00(CmdLine):
    
    
    def init(self,
             prog = 'cmd_test_prog',
             ):
        pass

    def build_parser(self):
        
        self.define_argument('engine',         
                             help='a positional arg',
                             completion_func=CmdLineTestClass00._completion_engine,)
        
        self.define_argument('when',         
                             help='another positional arg',
                             choices=['now', 'soon'])
        
        self.define_argument('--dir',         
                             help='directory',
                             completion_func=self._complete_dir_path)
        
        
        
        
        self.define_argument('--speed', '-s',  
                             help='an optional arg',
                             completion_func = self._completion_speed,
                             subcmds=['start'])

        self.define_argument('--sun-glass', '-S',  
                             help='an optional arg',
                             )

        
        self.define_argument('--gear', '-g',  
                             choices = [1,2,3,4],
                             type=int,
                             help='another optional arg')
        
        self.define_argument('--helmet', '-H', 
                             action='store_true', 
                             help='a switch')
        
        self.define_argument('--dests', '-d', 
                             nargs = '*', 
                             choices = list(range(19,23)),
                             help='destinations')
        
        
        ##
        self.add_arguments_main('--dir')
        
        #
        self.add_subcmd('start', 
                        help      = 'start help', 
                        func      = self.start,
                        arguments = (  
                            'engine',
                            'when', 
                            '--speed',
                            '--sun-glass',
                            '--helmet',
                            '--gear',
                            '--dests',
                        )
        )
        
    
        self.add_subcmd('stop', 
                        help = 'stop help',
                        func = self.stop,
                        arguments = (
                            'engine', 
                            'when', 
                            '--sun-glass'
                        )
        )
    
    

    
    def start(self,args):
        pass
    
    def stop(self,args):
        pass
        
    def _completion_engine(self,):
        return ['steam', 'otto']
    
    def _completion_speed(self,a,b):
        return ['10,40,100']
    
   