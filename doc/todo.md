# TODO #

### cut ###

- remove mixedtools.Loader?
- separate yaml module
- writefile with atomic write via tmp-file

### scmd ###

- prepare prog-dir (can be used a conf-dir too) and use it when 
  applying preset args 
  - any conflict with instances?

- bug: apply completion does not work with three positional args

- instead help as dict mapping subcmd to help text
```
    help='tuple to be replace'
    subcmds=['join_tt'],
```
   
- completion frame work overwork
    - match completion_func arguments by argument-names?

### utest ###

- utest.discovery with allow . as sub-dir key
- list with filtering by pattern likewise run
- reference test (not only cases) in run (and list)

### cleartrace ###

- make xmlrunner tee stdout
- show-locals with optional type info?

### shell ###

- case shell_00 with test on cplr into vs as mode

## techniques ##
 
- [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

- [Learn Hg](https://www.mercurial-scm.org/guide)
